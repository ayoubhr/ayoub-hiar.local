-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 21-11-2021 a las 22:17:08
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ayoub-hiar_servidores`
--
CREATE DATABASE IF NOT EXISTS `ayoub-hiar_servidores` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci;
USE `ayoub-hiar_servidores`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `tipo` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`tipo`, `id`) VALUES
('Pull', 22),
('Push', 23),
('Piernas', 24),
('equilibrio', 25),
('Push&amp;Equilibrio', 26),
('Power', 27);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--

CREATE TABLE `comentario` (
  `usuario_id` int(11) NOT NULL,
  `texto` text COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `rutina_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `comentario`
--

INSERT INTO `comentario` (`usuario_id`, `texto`, `rutina_id`, `date`, `id`) VALUES
(14, 'Me he suscrito a tu rutina Maria!!', 78, '2021-11-21 21:51:15', 21),
(14, 'Me parece una rutina bastante decente Dani, sigue asi!', 75, '2021-11-21 21:52:36', 22),
(12, 'Me uno a Ayoub y me suscribo you también!!', 78, '2021-11-21 21:55:11', 23),
(13, 'Hola, soy Maria, me suscribo Ayoub!!', 83, '2021-11-21 22:10:08', 24),
(13, 'Hola Dani!!, me interesan estos ejercicios isometricos, los probaré a ver que tal. Un saludo, Maria.', 76, '2021-11-21 22:11:11', 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

CREATE TABLE `imagenes` (
  `nombre` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `fullName` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `message` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `phone` varchar(9) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `mensajes`
--

INSERT INTO `mensajes` (`fullName`, `message`, `email`, `phone`, `id`) VALUES
('Ayoub Hiar', 'ayoub_hrrr@hotmail.com', 'Hola!!, quisiera saber si el formulario de contacto funciona correctamente!!', '634822423', 50);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `miembro`
--

CREATE TABLE `miembro` (
  `usuario_id` int(11) NOT NULL,
  `cursos_suscritos` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan_entreno`
--

CREATE TABLE `plan_entreno` (
  `imagen` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `ejercicio_uno` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `ejercicio_dos` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `ejercicio_tres` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `ejercicio_cuatro` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `ejercicio_cinco` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `usuarioCreador_id` int(11) NOT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `plan_entreno`
--

INSERT INTO `plan_entreno` (`imagen`, `nombre`, `ejercicio_uno`, `ejercicio_dos`, `ejercicio_tres`, `ejercicio_cuatro`, `ejercicio_cinco`, `descripcion`, `usuarioCreador_id`, `tipo`, `id`) VALUES
('12_back-muscle.jpg', 'Back Builder', 'Dominadas', 'Remo', 'FrontLever', 'Backlever', 'Abs', 'Dominadas 4x15\r\nRemo 4x15\r\nFrontlever 4x15\'\r\nBacklever 4x15\'\r\nAbs 4x15\r\n\r\nCircuito con descansos de 3 minutos entre vueltas.', 12, 'Pull', 75),
('12_frontlever.jpg', 'Front Lever', 'Icecreams', 'Dragonflags', 'Leglifts', 'tucklever', 'remo', 'Icrecreams 4x15\r\nDragonflags 4x15\r\nleglifts 4x15\r\ntucklever 4x15\'\r\nremo 4x15\r\n\r\n3 minutos de descanso en cada vuelta', 12, 'Pull', 76),
('12_backlever.png', 'Back Lever', 'Dominadas', 'Skin the cat', 'DragonFlag', 'Lumbares', 'Leglift', 'Dominadas 4x15\r\nSkin the cat 4x15\r\nDragonflag 4x15\r\nLumbares 4x30\r\nleglift 4x15\r\n\r\nCircuit, 3 minutos de descanso entre vueltas', 12, 'Pull', 77),
('13_pushUp.jpeg', 'Chest Builder', 'PushUps', 'Diamondpushups', 'Militarpushups', 'WallHandstand', 'LilyPushUps', '4x15 all of the excercises\r\n\r\nSin descansos', 13, 'Push', 78),
('13_pistolSquat.png', 'Pistol Squat', 'Squat', 'AssistedSquat', 'HIpThrust', 'Zancadas', 'Sprints', 'Squat 4x15\r\nAsisted Squat 4x15 cada pierna\r\nHip Thrust 4x20\r\nZancadas 4x10 cada pierna\r\nSprints 5x100m\r\n\r\n1 minutos de descanso entre cada ejercicio', 13, 'Piernas', 79),
('13_glutes.jpeg', 'Butt Builder', 'HIpThrust', 'JumpingSquats', 'Comba', 'Zancadas', 'Sentadillas', 'HIp Thrust 4x15\r\nJUmping Squats 4x10\r\nComba 6x60\'\r\nZancadas 4x20 cada pierna\r\nSentadillas 4x30\r\n\r\nCircuito, 5 minutos de descanso entre cada serie.', 13, 'Piernas', 80),
('14_handt.jpeg', 'Handstand master', 'WallHandstand', 'HollowBody', 'DragonFlag', 'Dominadas', 'HandstandKicks', 'Wall handstand 4x60\'\r\nHollow Body 4x120\'\r\nDragonflag 4x20\r\nDominadas 4x15\r\nHandstand kicks 4x5\r\n\r\n1 minutos de descanso entre cada ejercicio.', 14, 'equilibrio', 81),
('14_fullplanche.jpeg', 'Full Planche', 'PseudoPushUps', 'TuckHold', 'HSPU', 'Dragonflags', 'frontlever', 'Pseudo push ups 4x15\r\nTuck holds 4x20\'\r\nHSPU 4x4\r\ndragonflags 4x15\r\nfrontlever 4x15\'', 14, 'Push&amp;Equilibrio', 82),
('14_img_5.jpg', 'Power Builder', 'DeadLift', 'Squat', 'BenchPress', 'Remo', 'Leglift', 'Deadlift 4x5x150kg\r\nSquat 4x50\r\nBenchPress 4x20x100kg\r\nRemo 4x15\r\nLeglift 4x15\r\n\r\nCircuitos con un minuto de descanso entre ejercicios y 5 minutos entre series.', 14, 'Power', 83);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `suscripcion`
--

CREATE TABLE `suscripcion` (
  `id_usuario` int(11) NOT NULL,
  `id_plan_entreno` int(11) NOT NULL,
  `fecha_inscripcion` datetime NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `suscripcion`
--

INSERT INTO `suscripcion` (`id_usuario`, `id_plan_entreno`, `fecha_inscripcion`, `id`) VALUES
(14, 78, '2021-11-21 00:00:00', 12),
(14, 75, '2021-11-21 00:00:00', 13),
(12, 78, '2021-11-21 00:00:00', 14),
(13, 83, '2021-11-21 00:00:00', 15),
(13, 76, '2021-11-21 00:00:00', 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `email` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `fecha_alta` date DEFAULT NULL,
  `telefono` varchar(9) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `direccion` text COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `rol` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`email`, `password`, `nombre`, `apellidos`, `fecha_alta`, `telefono`, `direccion`, `rol`, `id`) VALUES
('admin@admin.com', '$2y$10$Ip2TL9GtyPdhv5PP0nCXB.q/MvNtHr7C45KOqhoVjBh9oz77van7K', 'admin', 'admin', '2021-11-21', '0000', 'admin', 'ROLE_ADMIN', 11),
('usuario_uno@hotmail.com', '$2y$10$IEurRbBPPkECHeq3czp/e.22HDHN.kZd0u8aP6Tuu9245LwRPNHiS', 'Daniel', 'Garcia', '2021-11-21', '612345789', 'calle penalva', 'ROLE_USER', 12),
('usuario_dos@hotmail.com', '$2y$10$MVT.VRKhX09vgKI1Cb6YDuLo7Tk.blEw5UYTmu9fmU9Adg5x39y8q', 'Maria', 'Gonzalez', '2021-11-21', '655879143', 'av los angeles', 'ROLE_USER', 13),
('usuario_tres@hotmail.com', '$2y$10$Kg1x/1aGsg15/XSCESbZYurN3gImERp.vq4t51SiKk0GUkCxkxDr.', 'Ayoub', 'Hiar', '2021-11-21', '677418523', 'av de madrid', 'ROLE_USER', 14);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categoria_id_uindex` (`id`);

--
-- Indices de la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `login_fk` (`usuario_id`),
  ADD KEY `comentario_fk_2` (`rutina_id`);

--
-- Indices de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mensajes_fk` (`email`);

--
-- Indices de la tabla `miembro`
--
ALTER TABLE `miembro`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `miembro_email_uindex` (`usuario_id`);

--
-- Indices de la tabla `plan_entreno`
--
ALTER TABLE `plan_entreno`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plan_entreno_fk_tipo` (`tipo`),
  ADD KEY `plan_entreno_fk` (`usuarioCreador_id`);

--
-- Indices de la tabla `suscripcion`
--
ALTER TABLE `suscripcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `suscripcion_fk_usuario` (`id_usuario`),
  ADD KEY `suscripcion_fk_rutina` (`id_plan_entreno`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usuario_password_uindex` (`password`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `comentario`
--
ALTER TABLE `comentario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de la tabla `miembro`
--
ALTER TABLE `miembro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `plan_entreno`
--
ALTER TABLE `plan_entreno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT de la tabla `suscripcion`
--
ALTER TABLE `suscripcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD CONSTRAINT `comentario_fk_2` FOREIGN KEY (`rutina_id`) REFERENCES `plan_entreno` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `login_fk` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `miembro`
--
ALTER TABLE `miembro`
  ADD CONSTRAINT `miembro_fk` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `plan_entreno`
--
ALTER TABLE `plan_entreno`
  ADD CONSTRAINT `plan_entreno_fk` FOREIGN KEY (`usuarioCreador_id`) REFERENCES `usuario` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `suscripcion`
--
ALTER TABLE `suscripcion`
  ADD CONSTRAINT `suscripcion_fk_rutina` FOREIGN KEY (`id_plan_entreno`) REFERENCES `plan_entreno` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `suscripcion_fk_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
