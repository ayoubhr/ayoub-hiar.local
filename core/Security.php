<?php

namespace proyecto\core;

use proyecto\app\exception\AppException;

class Security
{

    /**
     * @param string $role
     * @return bool
     * @throws AppException
     */
    public static function isUserGranted(string $role) : bool
    {

        if($role === 'ROLE_ANON'){
            return true;
        }

        $usuario = App::get('appUser');
        if(is_null($usuario)) {
            return false;
        }

        $valor_role = App::get('security')['roles'][$role];
        $valor_role_usuario = App::get('security')['roles'][$usuario->getRol()];

        return ($valor_role_usuario >= $valor_role);

    }

    /**
     * @param string $password
     * @param string $bdPassword
     * @return bool
     */
    public static function checkPassword(string $password, string $bdPassword) : bool
    {
        return password_verify($password, $bdPassword);
    }

}