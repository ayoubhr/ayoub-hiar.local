<?php
namespace proyecto\core;

use proyecto\app\Conecta;
use proyecto\app\repository\UsuarioEntityRepository;
use proyecto\app\utils\MyLog;
use proyecto\core\App;
use proyecto\core\Router;

session_start();

require __DIR__ . '/../vendor/autoload.php';

$config = Conecta::conecta();

$database = $config['database'];
App::bind('config', $database);

$router = Router::load(__DIR__ . '/../app/' . $config['routes']['filename']);
App::bind('router', $router);

$logger = MyLog::load(__DIR__ . '/../logs/' . $config['logs']['filename'], $config['logs']['level']);
App::bind('logger', $logger);

$project = $config['project']['namespace'];
App::bind('project', $project);

if(isset($_SESSION['loguedUser'])){
    $appUser = App::getRepository(UsuarioEntityRepository::class)->find($_SESSION['loguedUser']);
} else {
    $appUser = null;
}

App::bind('appUser', $appUser);

$security = $config['security'];
App::bind('security', $security);