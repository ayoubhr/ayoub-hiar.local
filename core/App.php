<?php
namespace proyecto\core;

use PDO;
use proyecto\app\exception\AppException;
use proyecto\core\database\Connection;
use proyecto\core\database\Querybuilder;

class App
{
    /**
     * @var array
     */
    private static $container = [];

    /**
     * @param string $key
     * @param $val
     */
    public static function bind(string $key, $val)
    {
        static::$container[$key] = $val;
    }

    /**
     * @param string $key
     * @return PDO
     * @throws AppException
     */
    public static function get(string $key)
    {
        if(!array_key_exists($key, static::$container)){
            throw new AppException("No se ha encontrado el objeto $key en el contenedor");
        }
        return static::$container[$key];
    }

    /**
     * @return PDO
     * @throws AppException
     */
    public static function getConnection()
    {
        if(!array_key_exists('connection', static::$container)){
            static::$container['connection'] = Connection::make();
        }
        return static::$container['connection'];
    }

    /**
     * @param string $className
     * @return Querybuilder
     */
    public static function getRepository(string $className) : Querybuilder
    {
        if(!array_key_exists($className, static::$container)){
            static::$container[$className] = new $className();
        }
        return static::$container[$className];
    }
}