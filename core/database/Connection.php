<?php
namespace proyecto\core\database;

use PDO;
use PDOException;
use proyecto\app\exception\AppException;
use proyecto\core\App;

class Connection
{
    /**
     * @return PDO
     * @throws AppException
     */
    public static function make(): PDO
    {
            $config = App::get('config');
            try {
                $connection = new PDO(
                    $config['connection'] . ';dbname=' . $config['name'],
                    $config['username'],
                    $config['password'],
                    $config['options']
                );
            } catch (PDOException $PDOException) {
                throw new AppException('No se ha podido conectar a la bd');
            }
        return $connection;
    }
}