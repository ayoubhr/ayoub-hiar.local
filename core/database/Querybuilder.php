<?php
namespace proyecto\core\database;

use PDO;
use PDOException;
use proyecto\app\exception\AppException;
use proyecto\app\exception\NotFoundException;
use proyecto\app\exception\QueryException;
use proyecto\core\App;

abstract class Querybuilder
{
    /**
     * @var PDO
     */
    private $connection;
    /**
     * @var string
     */
    private $table;
    /**
     * @var string
     */
    private $classEntity;

    /**
     * @throws AppException
     */
    public function __construct(string $table, string $classEntity)
    {
        $this->connection = App::getConnection();
        $this->table = $table;
        $this->classEntity = $classEntity;
    }

    /**
     * @param string $sql
     * @param array $parameters
     * @return array
     * @throws QueryException
     */
    private function executeQuery(string $sql, array $parameters=[]) : array
    {
        $pdoStatement = $this->connection->prepare($sql);

        if($pdoStatement->execute($parameters) === false) {
            throw new QueryException('No se ha podido ejecutar la sentencia findAll');
        }
        return $pdoStatement->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classEntity);
    }

    /**
     * @return array
     * @throws QueryException
     */
    public function findAll():array
    {
        $sql = "SELECT * FROM $this->table";

        return $this->executeQuery($sql);
    }

    /**
     * @param int $id
     * @return IEntity
     * @throws NotFoundException
     * @throws QueryException
     */
    public function find(int $id) : IEntity
    {
        $sql = "SELECT * FROM $this->table WHERE id = $id";

        $result = $this->executeQuery($sql);

        if(empty($result)){
            throw new NotFoundException("No se ha encontrado ningún elemento id $id");
        }
        return $result[0];
    }

    /**
     * @param int $id
     * @return array
     * @throws NotFoundException
     * @throws QueryException
     */
    public function fond(int $id) : array|null
    {
        $sql = "SELECT * FROM $this->table WHERE id_plan_entreno = $id";

        $result = $this->executeQuery($sql);

        if(empty($result)){
            return null;
        }
        return $result;
    }

    /**
     * @param array $filters
     * @return string
     */
    private function getFilters(array $filters){
        if(empty($filters)){
            return '';
        }

        $strFilters = [];

        foreach($filters as $key=>$value){
            $strFilters[] = $key . '=:' . $key;
        }

        return ' WHERE ' . implode(' and ', $strFilters);
    }

    /**
     * @param array $filters
     * @return array
     * @throws QueryException
     */
    public function findBy(array $filters) : array
    {
        $sql = "SELECT * FROM $this->table " . $this->getFilters($filters);

        return $this->executeQuery($sql, $filters);
    }

    /**
     * @param array $filters
     * @return IEntity|null
     * @throws QueryException
     */
    public function findOneBy(array $filters):?IEntity
    {
        $result = $this->findBy($filters);

        if(count($result) > 0){
            return $result[0];
        }
        return null;
    }

    /**
     * @param IEntity $entity
     * @throws QueryException
     */
    public function save(IEntity $entity):void
    {
        try {
            $parameters = $entity->toArray();
            $sql = sprintf(
                'insert into %s (%s) values (%s)',
                $this->table,
                implode(', ', array_keys($parameters)),
                ':' . implode(', :', array_keys($parameters))
            );
            $statement = $this->connection->prepare($sql);
            $statement->execute($parameters);
        } catch (PDOException $pdoException) {
            throw new PDOException('Error al insertar en la base de datos');
        } catch (QueryException $queryException) {
            throw new QueryException('error');
        }
    }

    /**
     * @param array $parameters
     * @return string
     */
    private function getUpdates(array $parameters)
    {
        $updates = '';

        foreach ($parameters as $key => $value){
            if ($key !== 'id') {
                if($updates !== ' '){

                    $updates .= $key . '=:' . $key . ', ';
                }
            }
        }
        return $updates;
    }

    /**
     * @param IEntity $entity
     * @throws QueryException
     */
    public function update(IEntity $entity, int $id):void
    {
        try {
            $parameters = $entity->toArray();
            $sql = sprintf(
                "UPDATE %s SET %sWHERE id=$id",
                $this->table, $this->getUpdates($parameters));

            $statement = $this->connection->prepare($sql);
            $statement->execute($parameters);
        } catch(PDOException $pdoException) {
            throw new QueryException('Error al actualizar el elemento con id ' . $parameters['id']);
        }
    }

    /**
     * @param IEntity $entity
     * @param int $id
     * @throws QueryException
     */
    public function updatePass(IEntity $entity, int $id)
    {
        try {
            $parameters = $entity->toArray();
            $password = $parameters['password'];
            $array = ['password' => $password, 'id' => $id];

            $sql = 'UPDATE usuario SET password=:password WHERE id=:id';

            $statement = $this->connection->prepare($sql);
            $statement->execute($array);
        } catch(PDOException $pdoException) {
            throw new QueryException('Error al actualizar el elemento con id ' . $parameters['id']);
        }
    }

    /**
     * @param IEntity $entity
     * @param int $id
     * @throws QueryException
     */
    public function delete(IEntity $entity, int $id):void {
        try{
            $sql = "DELETE * FROM $entity WHERE id = $id";
            $this->executeQuery($sql);
        } catch (PDOException $pdoException){
            throw new PDOException('Registro eliminado');
        }
    }

    /**
     * @param IEntity $entity
     * @param int $id
     * @throws QueryException
     */
    public function deleteUser(IEntity $entity, int $id):void {
        try{
            $sql = "DELETE $entity FROM usuario WHERE id = $id";
            $this->executeQuery($sql);
        } catch (PDOException $pdoException){
            throw new PDOException('Registro eliminado');
        }
    }

    public function deleteRutina(IEntity $entity, int $id):void {
        try{
            $sql = "DELETE $entity FROM plan_entreno WHERE id = $id";
            $this->executeQuery($sql);
        } catch (PDOException $pdoException){
            throw new PDOException('Registro eliminado');
        }
    }

    /**
     * @param IEntity $entity
     * @param int $id_usuario
     * @param int $id_plan_entreno
     * @throws QueryException
     */
    public function deleteSub(IEntity $entity, int $id_usuario, int $id_plan_entreno):void
    {
        $sql = "DELETE $entity FROM suscripcion WHERE id_usuario = $id_usuario AND id_plan_entreno = $id_plan_entreno";

        try{
            $this->executeQuery($sql);
        } catch (PDOException $pdoException){
            throw new PDOException('El registro no pudo ser eliminado');
        }
    }

    /**
     * @param callable $fnExecuteQueries
     * @throws QueryException
     */
    public function executeTransaction(callable $fnExecuteQueries)
    {
        try {
            $this->connection->beginTransaction();
            $fnExecuteQueries;
            $this->connection->commit();
        } catch (PDOException $pdoException){
            $this->connection->rollBack();
            throw new QueryException('No se ha podido realizar la transacción');
        }
    }

    /**
     * Para recibir el email del usuario.
     *
     *
     * @param string $email
     * @return array
     * @throws NotFoundException
     * @throws QueryException
     */
    public function findEmail(string $email) : array
    {
        $sql = "SELECT * FROM `ayoub-hiar_servidores` . `usuario` WHERE `email` = '$email'";

        $result = $this->executeQuery($sql);

        if(empty($result)){
            throw new NotFoundException("No se ha encontrado el email en la bd.");
        }
        return $result;
    }

    /**
     * Para devolver el tipo de la rutina
     *
     *
     * @return array
     * @throws QueryException
     */
    public function devuelveTipo() : array
    {
        $sql = "SELECT `tipo` FROM $this->table";

        return $this->executeQuery($sql);
    }
}