<?php

namespace proyecto\core\helpers;

use Exception;
use proyecto\core\App;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class Swiftmailer
{

    public static function mailer(string $toAddress, string $mensaje)
    {

        try {

            $swiftmailer = App::get('swiftmail');

            $transport = (new Swift_SmtpTransport('smtp.gmail.com', '587', 'tls'))
                                                ->setUsername('proyectoDeClase7777@gmail.com')
                                                ->setPassword('dwes7777')
            ;

            $mailer = new Swift_Mailer($transport);

            $message = new Swift_Message('Envío automático');
            $message->setFrom('proyectoDeClase7777@gmail.com');
            $message->addTo($toAddress);
            $message->setBody($mensaje);

            $result = $mailer->send($message);

        } catch(Exception $e) {
            echo $e->getMessage();
        }

    }

}