<?php

use proyecto\core\Router;

$router = new Router();

//pages
$router->get('', 'PagesController@index');
$router->get('about', 'PagesController@about');
$router->get('contact', 'PagesController@contacto');
$router->post('check_contacto', 'PagesController@check_contacto');

//registros
$router->get('register', 'RegisterController@register');
$router->post('register', 'RegisterController@check_register');

//Crear rutinas
$router->post('crear_rutina', 'RutinaController@crear_rutina', 'ROLE_USER');
$router->get('crear_rutina', 'RutinaController@crear_rutina', 'ROLE_USER');
$router->get('check_rutina', 'RutinaController@check_rutina');

//login
$router->get('login', 'AuthController@login');
$router->post('check-login', 'AuthController@checkLogin');
$router->get('logout', 'AuthController@logout', 'ROLE_USER');

//detalles rutina
$router->get('detalles_rutina/:id', 'RutinaController@detalles_rutina');

//comentarios
$router->get('detalles_rutina/comentario', 'ComentariosController@comentario', 'ROLE_USER');
$router->post('detalles_rutina/confirma_comentario', 'ComentariosController@confirma_comentario', 'ROLE_USER');


//suscripciones
$router->get('detalles_rutina/suscribete', 'SuscripcionesController@suscribete', 'ROLE_USER');
$router->post('detalles_rutina/confirma_suscripcion', 'SuscripcionesController@confirma_suscripcion', 'ROLE_USER');

//Eliminar suscripciones
$router->get('detalles_rutina/unsuscribete', 'SuscripcionesController@unsuscribete', 'ROLE_USER');
$router->post('detalles_rutina/confirma_unsuscripcion', 'SuscripcionesController@confirma_unsuscripcion', 'ROLE_USER');

//Perfil del usuario
$router->get('miperfil', 'PerfilUsuario@miperfil', 'ROLE_USER');
$router->Post('cambioPass', 'PerfilUsuario@cambioPass', 'ROLE_USER');
$router->get('mis_suscripciones', 'PerfilUsuario@mis_suscripciones', 'ROLE_USER');
$router->get('mis_rutinas', 'PerfilUsuario@mis_rutinas', 'ROLE_USER');
$router->get('elimina_rutina/:id', 'PerfilUsuario@elimina_rutina', 'ROLE_USER');
$router->post('confirm_delete_rutina/:id', 'PerfilUsuario@confirm_delete_rutina', 'ROLE_USER');
$router->get('actividad_usuario', 'PerfilUsuario@actividad_usuario', 'ROLE_USER');

//Admin
$router->get('administrador', 'AdminController@administrador', 'ROLE_ADMIN');
$router->get('info_page/:id', 'AdminController@info_page', 'ROLE_ADMIN');
$router->get('info_page/delete_user/:id', 'AdminController@delete_user', 'ROLE_ADMIN');
$router->post('confirm_delete/:id', 'AdminController@confirm_delete', 'ROLE_ADMIN');

//articulo
$router->get('articulos', 'ArticuloController@articulos_page', 'ROLE_USER');
$router->post('crear_articulo', 'ArticuloController@crear_articulo', 'ROLE_USER');
