<?php
namespace proyecto\app;

use Monolog\Logger;
use PDO;

class Conecta {
    public static function conecta()
    {
        return [
            'database' => [
                'name' => "ayoub-hiar_servidores",
                'username' => "userApp",
                'password' => "dwes",
                'connection' => "mysql:host=ayoub-hiar.local",
                'options' => [
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_PERSISTENT => true
                ]
            ],
            'logs' => [
                'filename' => 'proyecto.log',
                'level' => Logger::WARNING
            ],
            'routes' => [
                'filename' => 'routes.php'
            ],
            'project' => [
                'namespace' => 'proyecto'
            ],
            'security' => [
                'roles' => [
                    'ROLE_ADMIN' => 3,
                    'ROLE_USER' => 2,
                    'ROLE_ANON' => 1
                ]
            ],
        ];
    }
}