<?php
namespace proyecto\app\repository;

use proyecto\app\entity\ContactEntity;
use proyecto\app\exception\AppException;
use proyecto\core\database\Querybuilder;

class ContactEntityRepository extends Querybuilder
{

    /**
     * @param string $table
     * @param string $classEntity
     * @throws AppException
     */
    public function __construct(string $table='mensajes', string $classEntity=ContactEntity::class)
    {
        parent::__construct($table, $classEntity);
    }

}