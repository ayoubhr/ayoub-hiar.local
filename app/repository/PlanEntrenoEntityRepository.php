<?php
namespace proyecto\app\repository;

use proyecto\app\entity\PlanEntrenoEntity;
use proyecto\app\exception\AppException;
use proyecto\app\exception\NotFoundException;
use proyecto\core\database\IEntity;
use proyecto\core\database\Querybuilder;

class PlanEntrenoEntityRepository extends Querybuilder
{

    /**
     * @param string $table
     * @param string $classEntity
     * @throws AppException
     */
    public function __construct(string $table='plan_entreno', string $classEntity=PlanEntrenoEntity::class)
    {
        parent::__construct($table, $classEntity);
    }

    /*public function guardaTipo(CategoriaEntity $categoriaEntity){

        $fnGuardaTipo = function () use ($categoriaEntity) {

            $this->save($categoriaEntity);
        };
        $this->executeTransaction($fnGuardaTipo);
    }*/
}