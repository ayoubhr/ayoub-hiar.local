<?php
namespace proyecto\app\repository;

use proyecto\app\entity\ImagenGallery;
use proyecto\app\exception\AppException;
use proyecto\core\database\Querybuilder;

class ImagenGalleryRepository extends Querybuilder
{
    /**
     * @param string $table
     * @param string $classEntity
     * @throws AppException
     */
    public function __construct(string $table='imagenes', string $classEntity=ImagenGallery::class)
    {
        parent::__construct($table, $classEntity);
    }
}