<?php
namespace proyecto\app\repository;

use proyecto\app\entity\SuscripcionEntity;
use proyecto\app\exception\AppException;
use proyecto\core\database\Querybuilder;

class SuscripcionEntityRepository extends Querybuilder
{

    /**
     * @param string $table
     * @param string $classEntity
     * @throws AppException
     */
    public function __construct(string $table='suscripcion', string $classEntity=SuscripcionEntity::class)
    {
        parent::__construct($table, $classEntity);
    }

}