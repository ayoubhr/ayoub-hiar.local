<?php

namespace proyecto\app\repository;

use proyecto\app\entity\ComentarioEntity;
use proyecto\app\exception\AppException;
use proyecto\core\database\Querybuilder;

class ComentarioEntityRepository extends Querybuilder
{

    /**
     * @param string $table
     * @param string $classEntity
     * @throws AppException
     */
    public function __construct(string $table='comentario', string $classEntity=ComentarioEntity::class)
    {
        parent::__construct($table, $classEntity);
    }
}