<?php

namespace proyecto\app\repository;

use proyecto\app\entity\ArticuloEntity;
use proyecto\app\entity\PlanEntrenoEntity;
use proyecto\app\exception\AppException;
use proyecto\core\database\Querybuilder;

class ArticuloEntityRepository extends Querybuilder
{
    /**
     * @param string $table
     * @param string $classEntity
     * @throws AppException
     */
    public function __construct(string $table='articulo', string $classEntity=ArticuloEntity::class)
    {
        parent::__construct($table, $classEntity);
    }
}