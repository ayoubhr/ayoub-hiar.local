<?php
namespace proyecto\app\repository;

use proyecto\app\entity\CategoriaEntity;
use proyecto\app\entity\PlanEntrenoEntity;
use proyecto\app\exception\AppException;
use proyecto\app\exception\NotFoundException;
use proyecto\app\exception\QueryException;
use proyecto\core\database\Querybuilder;

class CategoriaEntityRepository extends Querybuilder
{

    /**
     * @param string $table
     * @param string $classEntity
     * @throws AppException
     */
    public function __construct(string $table='categoria', string $classEntity=CategoriaEntity::class)
    {
        parent::__construct($table, $classEntity);
    }

    /**
     * @param PlanEntrenoEntity $planEntrenoEntity
     * @return CategoriaEntity
     * @throws NotFoundException
     * @throws QueryException
     */
    public function getCategoria(PlanEntrenoEntity $planEntrenoEntity) : CategoriaEntity
    {
        $categoriaEntityRepository = new CategoriaEntityRepository();

        return $categoriaEntityRepository->find($planEntrenoEntity->getTipo());
    }
}