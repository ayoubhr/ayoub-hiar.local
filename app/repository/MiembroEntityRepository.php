<?php
namespace proyecto\app\repository;

use proyecto\app\entity\MiembroEntity;
use proyecto\app\exception\AppException;
use proyecto\core\database\Querybuilder;

class MiembroEntityRepository extends Querybuilder
{

    /**
     * @param string $table
     * @param string $classEntity
     * @throws AppException
     */
    public function __construct(string $table='miembro', string $classEntity=MiembroEntity::class)
    {
        parent::__construct($table, $classEntity);
    }

}