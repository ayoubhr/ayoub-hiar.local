<?php
namespace proyecto\app\repository;

use proyecto\app\entity\UsuarioEntity;
use proyecto\app\exception\AppException;
use proyecto\app\exception\QueryException;
use proyecto\core\database\IEntity;
use proyecto\core\database\Querybuilder;

class UsuarioEntityRepository extends Querybuilder
{

    /**
     * @param string $table
     * @param string $classEntity
     * @throws AppException
     */
    public function __construct(string $table='usuario', string $classEntity=UsuarioEntity::class)
    {
        parent::__construct($table, $classEntity);
    }

}