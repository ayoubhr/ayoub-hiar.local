<?php

namespace proyecto\app\controllers;

use proyecto\app\exception\NotFoundException;
use proyecto\app\exception\ValidationException;
use proyecto\app\repository\ComentarioEntityRepository;
use proyecto\app\repository\PlanEntrenoEntityRepository;
use proyecto\app\repository\SuscripcionEntityRepository;
use proyecto\app\repository\UsuarioEntityRepository;
use proyecto\core\App;
use proyecto\core\helpers\FlashMessage;
use proyecto\core\Response;
use proyecto\core\Security;

class PerfilUsuario
{

    public function miperfil()
    {
        $user = App::get('appUser');
        Response::renderView('miperfil', 'layout', compact('user'));
    }

    public function mis_suscripciones()
    {
        $rutinaRepo = App::getRepository(PlanEntrenoEntityRepository::class);

        $user = App::get('appUser');

        $id_user = $user->getId();

        $suscripcionesRepo = App::getRepository(SuscripcionEntityRepository::class);

        $arraySuscripciones = $suscripcionesRepo->findAll();

        $arrayElementos = [];

        foreach($arraySuscripciones as $suscripcion){
            if($suscripcion->getIdUsuario() === $id_user){
                $id_rutina = $suscripcion->getIdPlanEntreno();
                $arrayElementos[] = $rutinaRepo->find($id_rutina);
            }
        }

        Response::renderView('mis_suscripciones', 'layout', compact('user', 'arrayElementos'));
    }

    public function mis_rutinas()
    {
        $rutinaRepo = App::getRepository(PlanEntrenoEntityRepository::class);

        $user = App::get('appUser');

        $id_user = $user->getId();

        $arrayRutinas = $rutinaRepo->findAll();

        $arrayElementos = [];

        foreach($arrayRutinas as $rutina){
            if($rutina->getUsuarioCreadorId() === $id_user){
                $arrayElementos[] = $rutina;
            }
        }
        Response::renderView('mis_rutinas', 'layout', compact('user', 'arrayElementos'));
    }

    public function elimina_rutina($id)
    {
        $rutinaRepo = App::getRepository(PlanEntrenoEntityRepository::class);
        $rutina = $rutinaRepo->find($id);

        Response::renderView('elimina_rutina', 'layout', compact('rutina', 'id'));
    }

    public function confirm_delete_rutina($id)
    {
        try {

            if($_SERVER['REQUEST_METHOD'] === 'POST'){

                $eleccion = trim(htmlspecialchars($_POST['eleccion']));

                if(empty($eleccion)){
                    throw new NotFoundException('elige una opción:');
                } else {
                    if ($eleccion === 'si') {
                        $rutinaRepo = App::getRepository(PLanEntrenoEntityRepository::class);

                        $rutina = $rutinaRepo->find($id);

                        $rutinaRepo->deleteRutina($rutina, (int)$id);

                        $mensaje = "Se ha eliminado la rutina " . $rutina->getNombre() . ' con id ' . $id;
                        App::get('logger')->add($mensaje);


                        App::get('router')->redirect("mis_rutinas");
                    } else {
                        App::get('router')->redirect("mis_rutinas");
                    }

                }
            }

        } catch(NotFoundException $notFoundException) {
            FlashMessage::set('delete-error', [$notFoundException->getMessage()]);

            App::get('router')->redirect("rutinas");
        }
    }

    public function actividad_usuario()
    {
        $comentariosRepo = App::getRepository(ComentarioEntityRepository::class);

        $rutinaRepo = App::getRepository(PlanEntrenoEntityRepository::class);

        $user = App::get('appUser');

        $id_user = $user->getId();

        $arrayComentarios = $comentariosRepo->findAll();

        $arrayElementos = [];

        $arrayRutinas = [];

        $id_rutina = 0;

        foreach($arrayComentarios as $comentario){
            if($comentario->getUsuarioId() === $id_user){
                $arrayElementos[] = $comentario;
                $id_rutina = $comentario->getRutinaId();
                $rutina = $rutinaRepo->find($id_rutina);
                $arrayRutinas[] = $rutina->getNombre();
            }
        }
        Response::renderView('mis_comentarios', 'layout', compact('user', 'arrayElementos', 'id_rutina', 'arrayRutinas'));
    }

    public function cambioPass()
    {
        try {
            $user = App::get('appUser');
            $userRepo = App::getRepository(UsuarioEntityRepository::class);
            $password = trim(htmlspecialchars($_POST['password']));
            $newPassword = trim(htmlspecialchars($_POST['newPassword']));
            $mensaje = "";

            if(!isset($_POST['password']) || empty($_POST['password'])) {
                throw new ValidationException('Debes introducir el email y password');
            }

            if(!isset($_POST['newPassword']) || empty($_POST['newPassword'])) {
                throw new ValidationException('Debes introducir el email y password');
            }

            if(Security::checkPassword($password, $user->getPassword())){
                $user->setPassword(password_hash($newPassword, PASSWORD_BCRYPT));
                $userRepo->updatePass($user, $user->getId());

                $mensaje = "El usuario " . $user->getNombre() . ' ' . $user->getApellidos() . ' ha modificado su contraseña';
                App::get('logger')->add($mensaje);
            }

            App::get('router')->redirect('miperfil');
        } catch(ValidationException $validationException) {
            FlashMessage::set('perfil-error', [$validationException->getMessage()]);
            App::get('router')->redirect('miperfil');
        }
    }
}