<?php
namespace proyecto\app\controllers;

use proyecto\app\entity\ContactEntity;
use proyecto\app\exception\QueryException;
use proyecto\app\exception\ValidationException;
use proyecto\app\repository\ContactEntityRepository;
use proyecto\app\repository\PlanEntrenoEntityRepository;
use proyecto\app\repository\UsuarioEntityRepository;
use proyecto\core\App;
use proyecto\core\helpers\FlashMessage;
use proyecto\core\helpers\Swiftmailer;
use proyecto\core\Response;

class PagesController
{
    /**
     * @throws QueryException
     */
    public function index()
    {
        $arrayElementos = [];
        $arrayRestoElementos = [];
        $usuariosPrincipales = [];
        $restoUsuarios = [];

        $rutinaRepository = App::getRepository(PlanEntrenoEntityRepository::class);
        //$usuario = App::getRepository(UsuarioEntityRepository::class)->find(App::get('appUser')->getId());

        $arrayElementos = array_slice($rutinaRepository->findAll(), -6);
        $arrayRestoElementos = array_slice($rutinaRepository->findAll(), 0, count($rutinaRepository->findAll())-count($arrayElementos));

        foreach($arrayElementos as $elemento){
            $usuariosPrincipales[] = App::getRepository(UsuarioEntityRepository::class)->find($elemento->getUsuarioCreadorId());
        }

        foreach($arrayRestoElementos as $elemento){
            $restoUsuarios[] = App::getRepository(UsuarioEntityRepository::class)->find($elemento->getUsuarioCreadorId());
        }

        Response::renderView('index', 'layout', compact('arrayElementos', 'arrayRestoElementos', 'usuariosPrincipales', 'restoUsuarios'));
    }

    public function about()
    {
        Response::renderView('about', 'layout');
    }

    public function contacto()
    {
        $errores = FlashMessage::get('contact-error', []);

        Response::renderView('contact', 'layout', compact('errores'));
    }

    public function check_contacto()
    {

        try {

            if ($_SERVER['REQUEST_METHOD'] === "POST") {

                $fullName = trim(htmlspecialchars($_POST['fullname']));
                $phone = trim(htmlspecialchars($_POST['phone']));
                $email = trim(htmlspecialchars($_POST['email']));
                $message = trim(htmlspecialchars($_POST['message']));

                if (empty($fullName) || empty($email) || empty($message)) {
                    if (empty($fullName) && empty($email) && empty($message)) {
                        throw new ValidationException('Debes rellenar los datos de contacto');
                    } else {
                        if (empty($fullName)) {
                            throw new ValidationException('Debes incluir tu nombre');
                        }
                        if (empty($message)) {
                            throw new ValidationException('Debes incluir tu mensaje');
                        }
                        if (empty($email)) {
                            throw new ValidationException('Debes incluir tu email');
                        }
                    }
                }
                if (!empty($email)) {
                    if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                        throw new ValidationException('Debes incluir un correo válido');
                    }
                    $contacto = new ContactEntity($fullName, $email, $message, $phone);
                    $contactRepository = App::getRepository(ContactEntityRepository::class);
                    $contactRepository->save($contacto);

                    $mensaje = "hemos recibido un nuevo mensaje de contacto";
                    App::get('logger')->add($mensaje);

                    $mensaje = "tu mensaje ha sido enviado correctamente";

                    Swiftmailer::mailer($email, $mensaje);

                    Response::renderView('contact', 'layout', compact('mensaje'));

                    App::get('router')->redirect('contact');
                }
            }

        } catch (ValidationException $validationException) {
            FlashMessage::set('contact-error', [$validationException->getMessage()]);

            App::get('router')->redirect('contact');
        }

    }
}