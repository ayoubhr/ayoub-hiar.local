<?php

namespace proyecto\app\controllers;

use proyecto\app\entity\SuscripcionEntity;
use proyecto\app\exception\NotFoundException;
use proyecto\app\repository\PlanEntrenoEntityRepository;
use proyecto\app\repository\SuscripcionEntityRepository;
use proyecto\app\repository\UsuarioEntityRepository;
use proyecto\core\App;
use proyecto\core\helpers\FlashMessage;
use proyecto\core\helpers\Swiftmailer;
use proyecto\core\Response;

class SuscripcionesController
{

    public function confirma_suscripcion()
    {
        try {
            if($_SERVER['REQUEST_METHOD'] === 'POST'){

                $id_rutina = $_COOKIE['id_rutina'];
                $eleccion = trim(htmlspecialchars($_POST['eleccion']));

                if(empty($eleccion)){
                    throw new NotFoundException('elige una opción:');
                } else {
                    if ($eleccion === 'si') {
                        $usuario = App::get('appUser');

                        $suscripcionEntity = new SuscripcionEntity($usuario->getId(), (int)$_COOKIE['id_rutina']);

                        $suscripcionRepo = App::getRepository(SuscripcionEntityRepository::class);

                        $suscripcionRepo->save($suscripcionEntity);

                        $rutinaRepo = App::getRepository(PlanEntrenoEntityRepository::class);
                        $rut = $rutinaRepo->find($id_rutina);

                        $usuarioRepo = App::getRepository(UsuarioEntityRepository::class);
                        $name = $usuarioRepo->find($usuario->getId());

                        $mensaje = "El usuario " . $name->getNombre() . ' ' . $name->getApellidos() . ' se ha suscrito a la rutina ' . $rut->getNOmbre();
                        App::get('logger')->add($mensaje);

                        $usuarioCreador_id = $rut->getUsuarioCreadorId();
                        $usuarioCreador = $usuarioRepo->find($usuarioCreador_id);
                        $email = $usuarioCreador->getEmail();
                        Swiftmailer::mailer($email, $mensaje);

                        App::get('router')->redirect("detalles_rutina/$id_rutina");
                    } else {
                        App::get('router')->redirect("detalles_rutina/$id_rutina");
                    }
                }
            }
        } catch (NotFoundException $notFoundException){
            FlashMessage::set('suscripcion-error', [$notFoundException->getMessage()]);

            App::get('router')->redirect('detalles_rutina');
        }
    }

    public function suscribete()
    {
        $user = $user = App::get('appUser')->getNombre() . ' ' . App::get('appUser')->getApellidos();

        Response::renderView('suscripcion', 'layout', compact('user'));
    }
    
    /* Los dos métodos de abajo son para la des suscripcion */

    public function unsuscribete()
    {
        $user = $user = App::get('appUser')->getNombre() . ' ' . App::get('appUser')->getApellidos();

        Response::renderView('unsuscribe', 'layout', compact('user'));
    }

    public function confirma_unsuscripcion()
    {
        try {

            if($_SERVER['REQUEST_METHOD'] === 'POST'){

                $id_rutina = $_COOKIE['id_rutina'];
                $eleccion = trim(htmlspecialchars($_POST['eleccion']));

                if(empty($eleccion)){
                    throw new NotFoundException('elige una opción:');
                } else {
                    if ($eleccion === 'si') {
                        $usuario = App::get('appUser');

                        $suscripcionEntity = new SuscripcionEntity();

                        $suscripcionRepo = App::getRepository(SuscripcionEntityRepository::class);

                        $suscripcionRepo->deleteSub($suscripcionEntity, $usuario->getId(), (int)$_COOKIE['id_rutina']);

                        $rutinaRepo = App::getRepository(PlanEntrenoEntityRepository::class);
                        $rut = $rutinaRepo->find((int)$id_rutina);

                        $usuarioRepo = App::getRepository(UsuarioEntityRepository::class);
                        $name = $usuarioRepo->find($usuario->getId());

                        $mensaje = "El usuario " . $name->getNombre() . ' ' . $name->getApellidos() . ' se ha des-suscrito de la rutina ' . $rut->getNOmbre();
                        App::get('logger')->add($mensaje);

                        $usuarioCreador_id = $rut->getUsuarioCreadorId();
                        $usuarioCreador = $usuarioRepo->find($usuarioCreador_id);
                        $email = $usuarioCreador->getEmail();
                        Swiftmailer::mailer($email, $mensaje);

                        App::get('router')->redirect("detalles_rutina/$id_rutina");
                    } else {
                        App::get('router')->redirect("detalles_rutina/$id_rutina");
                    }

                }
            }

        } catch(NotFoundException $notFoundException) {
            FlashMessage::set('suscripcion-error', [$notFoundException->getMessage()]);

            App::get('router')->redirect('detalles_rutina');
        }
    }
}