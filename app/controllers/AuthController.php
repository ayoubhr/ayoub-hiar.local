<?php

namespace proyecto\app\controllers;

use proyecto\app\exception\AppException;
use proyecto\app\exception\QueryException;
use proyecto\app\exception\ValidationException;
use proyecto\app\repository\UsuarioEntityRepository;
use proyecto\core\App;
use proyecto\core\helpers\FlashMessage;
use proyecto\core\Response;
use proyecto\core\Security;

class AuthController
{
    /**
     *
     */
    public function login(){

        $errores = FlashMessage::get('login-error', []);
        $email = FlashMessage::get('email');

        Response::renderView('login', 'layout', compact('errores', 'email'));
    }

    /**
     * @throws AppException|QueryException
     */
    public function checkLogin(){
        try{
            if(!isset($_POST['email']) || empty($_POST['email'])) {
                throw new ValidationException('Debes introducir el email y password');
            }

            FlashMessage::set('email', $_POST['email']);

            if(!isset($_POST['password']) || empty($_POST['password']))
            {
                throw new ValidationException('Debes introducir el email y password');
            }

            $usuario = App::getRepository(UsuarioEntityRepository::class)->findOneBy([
                'email' => $_POST['email']
            ]);

            if(!is_null($usuario) && Security::checkPassword($_POST['password'], $usuario->getPassword())){
                $_SESSION['loguedUser'] = $usuario->getId();
                FlashMessage::unset('email');
                App::get('router')->redirect('');
            }
            throw new ValidationException('El usuario y password introducidos no son correctos');
        } catch(ValidationException $validationException){
            FlashMessage::set('login-error', [$validationException->getMessage()]);
            App::get('router')->redirect('login');
        }
    }

    public function logout(){
        if(isset($_SESSION['loguedUser'])){
            $_SESSION['loguedUser'] = null;
            unset($_SESSION['loguedUser']);
        }
        App::get('router')->redirect('login');
    }
}