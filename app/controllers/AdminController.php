<?php

namespace proyecto\app\controllers;

use proyecto\app\entity\SuscripcionEntity;
use proyecto\app\entity\UsuarioEntity;
use proyecto\app\exception\NotFoundException;
use proyecto\app\repository\ComentarioEntityRepository;
use proyecto\app\repository\PlanEntrenoEntityRepository;
use proyecto\app\repository\SuscripcionEntityRepository;
use proyecto\app\repository\UsuarioEntityRepository;
use proyecto\core\App;
use proyecto\core\helpers\FlashMessage;
use proyecto\core\helpers\Swiftmailer;
use proyecto\core\Response;

class AdminController
{
    public function administrador()
    {
        $user = App::get('appUser');

        $userRepo = App::getRepository(UsuarioEntityRepository::class);

        $usuarios = $userRepo->findAll();

        Response::renderView('administrador', 'layout', compact('user', 'usuarios'));
    }

    public function info_page($id)
    {
        //info del usuario
        $userRepo = App::getRepository(UsuarioEntityRepository::class);
        $user = $userRepo->find($id);

        //Rutinas creadas por el usuario
        $rutinaRepo = App::getRepository(PlanEntrenoEntityRepository::class);
        $arrayRutinas = $rutinaRepo->findAll();
        $arrayElementosRutinas = [];
        foreach($arrayRutinas as $rutina){
            if($rutina->getUsuarioCreadorId() === (int)$id){
                $arrayElementosRutinas[] = $rutina;
            }
        }

        //Comentarios realizados por el usuario
        $comentariosRepo = App::getRepository(ComentarioEntityRepository::class);
        $arrayComentarios = $comentariosRepo->findAll();
        $arrayElementosComentarios = [];
        foreach($arrayComentarios as $comentario) {
            if ($comentario->getUsuarioId() === (int)$id) {
                $arrayElementosComentarios[] = $comentario;
            }
        }
        //suscripciones del usuario
        $suscripcionesRepo = App::getRepository(SuscripcionEntityRepository::class);
        $arraySuscripciones = $suscripcionesRepo->findAll();
        $arrayElementosSuscripciones = [];
        foreach($arraySuscripciones as $suscripcion){
            if($suscripcion->getIdUsuario() === (int)$id){
                $arrayElementosSuscripciones[] = $suscripcion;
            }
        }

        Response::renderView('infopage', 'layout', compact('user', 'arrayElementosRutinas', 'arrayElementosComentarios', 'arrayElementosSuscripciones'));
    }

    public function delete_user($id)
    {
        $userRepo = App::getRepository(UsuarioEntityRepository::class);
        $user = $userRepo->find($id);

        Response::renderView('delete_user', 'layout', compact('user', 'id'));
    }

    public function confirm_delete($id)
    {
        try {

            if($_SERVER['REQUEST_METHOD'] === 'POST'){

                $eleccion = trim(htmlspecialchars($_POST['eleccion']));

                if(empty($eleccion)){
                    throw new NotFoundException('elige una opción:');
                } else {
                    if ($eleccion === 'si') {
                        $userRepo = App::getRepository(UsuarioEntityRepository::class);

                        $user = $userRepo->find($id);

                        $userRepo->deleteUser($user, (int)$id);

                        $mensaje = "Se ha eliminado al usuario " . $user->getNombre() . ' ' . $user->getApellidos() . ' con id ' . $id;
                        App::get('logger')->add($mensaje);


                        App::get('router')->redirect("administrador");
                    } else {
                        App::get('router')->redirect("info_page/$id");
                    }

                }
            }

        } catch(NotFoundException $notFoundException) {
            FlashMessage::set('delete-error', [$notFoundException->getMessage()]);

            App::get('router')->redirect("info_page/$id");
        }
    }
}