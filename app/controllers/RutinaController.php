<?php

namespace proyecto\app\controllers;

use proyecto\app\entity\ComentarioEntity;
use proyecto\app\entity\ImagenGallery;
use proyecto\app\exception\AppException;
use proyecto\app\exception\FileException;
use proyecto\app\exception\NotFoundException;
use proyecto\app\exception\QueryException;
use proyecto\app\entity\CategoriaEntity;
use proyecto\app\entity\PlanEntrenoEntity;
use proyecto\app\exception\ValidationException;
use proyecto\app\repository\CategoriaEntityRepository;
use proyecto\app\repository\ComentarioEntityRepository;
use proyecto\app\repository\PlanEntrenoEntityRepository;
use proyecto\app\repository\SuscripcionEntityRepository;
use proyecto\app\repository\UsuarioEntityRepository;
use proyecto\app\utils\File;
use proyecto\core\App;
use proyecto\core\helpers\FlashMessage;
use proyecto\core\Response;

class RutinaController
{

    /**
     * @throws AppException
     * @throws QueryException
     */
    public function crear_rutina(){
        $mensaje = '';
        $rutina = "";
        $rutinaRepository = App::getRepository(PlanEntrenoEntityRepository::class);

        try{

            if($_SERVER['REQUEST_METHOD'] === 'POST'){
                $nombre = trim(htmlspecialchars($_POST['nombre']));
                $ejercicio_uno = trim(htmlspecialchars($_POST['ejercicio_uno']));
                $ejercicio_dos = trim(htmlspecialchars($_POST['ejercicio_dos']));
                $ejercicio_tres = trim(htmlspecialchars($_POST['ejercicio_tres']));
                $ejercicio_cuatro = trim(htmlspecialchars($_POST['ejercicio_cuatro']));
                $ejercicio_cinco = trim(htmlspecialchars($_POST['ejercicio_cinco']));
                $descripcion = trim(htmlspecialchars($_POST['descripcion']));
                $tipo = trim(htmlspecialchars($_POST['tipo']));

                if (empty($nombre) || empty($ejercicio_uno) || empty($ejercicio_dos) ||
                    empty($ejercicio_tres) || empty($ejercicio_cuatro) || empty($ejercicio_cinco) ||
                    empty($descripcion) || empty($tipo))
                {
                    if (empty($nombre) && empty($ejercicio_uno) && empty($ejercicio_dos) &&
                        empty($ejercicio_tres) && empty($ejercicio_cuatro) && empty($ejercicio_cinco) &&
                        empty($descripcion) && empty($tipo))
                    {
                        throw new ValidationException('Debes introducir todos los campos necesarios');
                    } else {
                        if (empty($nombre)) {
                            throw new ValidationException('Debes incluir un nombre');
                        }
                        if (empty($ejercicio_uno)) {
                            throw new ValidationException('Debes incluir los cinco ejercicios');
                        }
                        if (empty($ejercicio_dos)) {
                            throw new ValidationException('Debes incluir los cinco ejercicios');
                        }
                        if (empty($ejercicio_tres)) {
                            throw new ValidationException('Debes incluir los cinco ejercicios');
                        }
                        if (empty($ejercicio_cuatro)) {
                            throw new ValidationException('Debes incluir los cinco ejercicios');
                        }
                        if (empty($ejercicio_cinco)) {
                            throw new ValidationException('Debes incluir los cinco ejercicios');
                        }
                        if (empty($descripcion)) {
                            throw new ValidationException('Debes detallar tu rutina');
                        }
                        if (empty($tipo)) {
                            throw new ValidationException('Debes indicar la especialidad');
                        }
                    }
                }

                $tiposAceptados = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];
                $imagen = new File('imagen', $tiposAceptados);

                $imagen->saveUploadFile(ImagenGallery::RUTA_IMAGEN_GALLERY);
                $imagen->copyFile(ImagenGallery::RUTA_IMAGEN_GALLERY, ImagenGallery::RUTA_IMAGEN_WEB);

                $rutina = new PlanEntrenoEntity($imagen->getFileName(), $nombre, $ejercicio_uno, $ejercicio_dos, $ejercicio_tres,
                    $ejercicio_cuatro, $ejercicio_cinco, $descripcion, App::get('appUser')->getId(), $tipo);
                $rutinaRepository = App::getRepository(PlanEntrenoEntityRepository::class);
                $rutinaRepository->save($rutina);

                $categoria = new CategoriaEntity($tipo);
                $categoriaRepository = App::getRepository(CategoriaEntityRepository::class);

                $array = $categoriaRepository->findAll();
                if(!in_array($tipo, $array)){
                    $categoriaRepository->save($categoria);
                }
                $mensaje = "Se ha añadido una nueva rutina a través del usuario " . App::get('appUser')->getId();
                App::get('logger')->add($mensaje);

                $mensaje = "Rutina creada correctamente!";
            }

            Response::renderView("crear_rutina", "layout", compact('rutina', 'rutinaRepository', 'mensaje'));

            App::get('router')->redirect('crear_rutina');

        } catch(ValidationException $validationException){
            FlashMessage::set('rutina-error', [$validationException->getMessage()]);

            App::get('router')->redirect('check_rutina');
        } catch (FileException $e) {
            FlashMessage::set('rutina-error', [$e->getMessage()]);

            App::get('router')->redirect('check_rutina');
        }

    }

    public function check_rutina(){
        $errores = FlashMessage::get('rutina-error', []);

        Response::renderView('crear_rutina', 'layout', compact('errores'));
    }

    public function detalles_rutina($id){

        $comentarioRepo = App::getRepository(ComentarioEntityRepository::class);
        $comentariosRutina = $comentarioRepo->findAll();

        $rutinaRepository = App::getRepository(PlanEntrenoEntityRepository::class);
        $usuarioRepository = App::getRepository(UsuarioEntityRepository::class);

        $detallesRutina = $rutinaRepository->find($id);
        $idd = $detallesRutina->getId();
        setcookie("id_rutina", $idd);

        $nombreUsuarioCreador = $usuarioRepository->find($detallesRutina->getUsuarioCreadorId());

        $nombre = $nombreUsuarioCreador->getNombre() . ' ' . $nombreUsuarioCreador->getApellidos();

        $suscripciones = App::getRepository(SuscripcionEntityRepository::class);

        $logged = App::get('appUser');

        $id_usuario_logged = 0;

        $usuario_esta_suscrito = " ";

        if(!is_null($logged)){

            $id_usuario_logged = $logged->getId();

            $filtros = ['id_usuario' => $id_usuario_logged, 'id_plan_entreno' => $idd];

            if(!is_null($suscripciones->findOneBy($filtros))){
                $usuario_esta_suscrito = "si";
            } else {
                $usuario_esta_suscrito = "no";
            }
        }

        if(is_null($suscripciones->fond($id))){
            $totalSuscripciones = 0;
            Response::renderView('detalles_rutina', 'layout', compact('comentariosRutina', 'detallesRutina', 'totalSuscripciones', 'nombreUsuarioCreador', 'nombre', 'logged', 'usuario_esta_suscrito'));
        } else {
            $totalSuscripciones = $suscripciones->fond($id);
            Response::renderView('detalles_rutina', 'layout', compact('comentariosRutina', 'detallesRutina', 'totalSuscripciones', 'nombreUsuarioCreador', 'nombre', 'logged', 'usuario_esta_suscrito'));
        }
        //App::get('router')->redirect('detalles_rutina');
    }
}