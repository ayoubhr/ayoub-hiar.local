<?php

namespace proyecto\app\controllers;

use proyecto\app\entity\ArticuloEntity;
use proyecto\app\entity\CategoriaEntity;
use proyecto\app\entity\ImagenGallery;
use proyecto\app\entity\PlanEntrenoEntity;
use proyecto\app\exception\FileException;
use proyecto\app\exception\ValidationException;
use proyecto\app\repository\ArticuloEntityRepository;
use proyecto\app\repository\CategoriaEntityRepository;
use proyecto\app\repository\PlanEntrenoEntityRepository;
use proyecto\app\repository\UsuarioEntityRepository;
use proyecto\app\utils\File;
use proyecto\core\App;
use proyecto\core\helpers\FlashMessage;
use proyecto\core\Response;

class ArticuloController
{

    public function articulos_page()
    {
        $errores = FlashMessage::get('errores-articulo', []);

        $articuloRepository = App::getRepository(ArticuloEntityRepository::class);

        $articulos = [];

        $user = App::get('appUser')->getId();

        if($user === 11){
            $articulos = $articuloRepository->findAll();
        } else {
            $articulos = App::getRepository(ArticuloEntityRepository::class)->findBy([
                'id_usuario' => $user
            ]);
        }

        Response::renderView('articulos', 'layout', compact('errores', 'articulos'));
    }

    public function crear_articulo()
    {
        $mensaje = '';
        $rutina = "";
        $articuloRepository = App::getRepository(ArticuloEntityRepository::class);

        try{

            if($_SERVER['REQUEST_METHOD'] === 'POST'){
                $nombre = trim(htmlspecialchars($_POST['nombre']));
                $precio = trim(htmlspecialchars($_POST['precio']));
                $fecha_caducidad = trim(htmlspecialchars($_POST['fecha_caducidad']));


                if (empty($nombre) || empty($precio) || empty($fecha_caducidad))
                {
                    if (empty($nombre) && empty($precio) && empty($fecha_caducidad))
                    {
                        throw new ValidationException('Debes introducir todos los campos necesarios');
                    } else {
                        if (empty($nombre)) {
                            throw new ValidationException('Debes incluir un nombre');
                        }
                        if (empty($precio)) {
                            throw new ValidationException('Debes incluir los cinco ejercicios');
                        }
                        if (empty($fecha_caducidad)) {
                            throw new ValidationException('Debes incluir los cinco ejercicios');
                        }
                    }
                }

                $tiposAceptados = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];
                $imagen = new File('imagen', $tiposAceptados);

                $imagen->saveUploadFile(ImagenGallery::RUTA_IMAGEN_GALLERY);
                $imagen->copyFile(ImagenGallery::RUTA_IMAGEN_GALLERY, ImagenGallery::RUTA_IMAGEN_WEB);

                $id_usuario = App::get('appUser')->getId();

                $articulo = new ArticuloEntity($nombre, $precio, $imagen->getFileName(), $id_usuario);

                $articuloRepository->save($articulo);

                $mensaje = "Se ha añadido un nuevo articulo a través del usuario " . $id_usuario;
                App::get('logger')->add($mensaje);

                $mensaje = "Rutina creada correctamente!";
            }

            Response::renderView("articulos", "layout", compact('rutina', 'mensaje'));

            App::get('router')->redirect('articulos');

        } catch(ValidationException $validationException){
            FlashMessage::set('errores-articulo', [$validationException->getMessage()]);

            App::get('router')->redirect('articulos');
        } catch (FileException $e) {
            FlashMessage::set('errores-articulo', [$e->getMessage()]);

            App::get('router')->redirect('articulos');
        }
    }
}