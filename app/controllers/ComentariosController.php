<?php

namespace proyecto\app\controllers;

use proyecto\app\entity\ComentarioEntity;
use proyecto\app\exception\NotFoundException;
use proyecto\app\repository\ComentarioEntityRepository;
use proyecto\app\repository\PlanEntrenoEntityRepository;
use proyecto\app\repository\UsuarioEntityRepository;
use proyecto\core\App;
use proyecto\core\helpers\FlashMessage;
use proyecto\core\helpers\Swiftmailer;
use proyecto\core\Response;

class ComentariosController
{

    public function confirma_comentario()
    {
        try {
            if($_SERVER['REQUEST_METHOD'] === 'POST'){
                $comentario = trim(htmlspecialchars($_POST['comentario']));

                if(empty($comentario)){
                    throw new NotFoundException('Y el texto del comment pa cuando?');
                } else {
                    $usuario = App::get('appUser');

                    $comentarioEntity = new ComentarioEntity($usuario->getId(), $comentario, (int) $_COOKIE['id_rutina']);

                    $comentarioRepo = App::getRepository(ComentarioEntityRepository::class);

                    $comentarioRepo->save($comentarioEntity);

                    $id_rutina = $_COOKIE['id_rutina'];

                    $rutinaRepo = App::getRepository(PlanEntrenoEntityRepository::class);
                    $rut = $rutinaRepo->find($id_rutina);
                    $usuarioCreador_id = $rut->getUsuarioCreadorId();

                    $usuarioRepo = App::getRepository(UsuarioEntityRepository::class);
                    $usuarioCreador = $usuarioRepo->find($usuarioCreador_id);
                    $email = $usuarioCreador->getEmail();

                    $mensaje = "El usuario " . $usuario->getNombre() . ' ' . $usuario->getApellidos() . ' ha comentado en tu rutina ' . $rut->getNombre();

                    Swiftmailer::mailer($email, $mensaje);
                    App::get('router')->redirect("detalles_rutina/$id_rutina");
                }
            }
        } catch (NotFoundException $notFoundException){
            FlashMessage::set('comentario-error', [$notFoundException->getMessage()]);

            App::get('router')->redirect('detalles_rutina');
        }
    }

    public function comentario()
    {
        $user = $user = App::get('appUser')->getNombre() . ' ' . App::get('appUser')->getApellidos();

        Response::renderView('comentario', 'layout', compact('user'));

    }

}