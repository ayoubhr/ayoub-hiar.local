<?php

use proyecto\app\entity\ImagenGallery;
use proyecto\app\exception\AppException;
use proyecto\app\exception\FileException;
use proyecto\app\exception\QueryException;
use proyecto\app\repository\ImagenGalleryRepository;
use proyecto\app\utils\File;
use proyecto\core\App;

require __DIR__ . '/../views/gallery.view.php';

$mensaje = "";
$erroresValidacion = [];
$descripcion = "";

$imagenes = [];

try {

    $imgRepository = App::getRepository(ImagenGalleryRepository::class);

    if ($_SERVER['REQUEST_METHOD'] === "POST") {

        $descripcion = trim(htmlspecialchars($_POST['descripcion']));

        $tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
        $imagen = new File('imagen', $tiposAceptados);
        $imagen->saveUploadFile(ImagenGallery::RUTA_IMAGEN_GALLERY);
        $imagen->copyFile(ImagenGallery::RUTA_IMAGEN_GALLERY, ImagenGallery::RUTA_IMAGEN_WEB);

        $imagenGallery = new ImagenGallery($imagen->getFileName(), $descripcion);
        $imgRepository->save($imagenGallery);

        $descripcion = '';
        $mensaje = "Se ha guardado la imagen " . $imagenGallery->getNombre() . " en la bd";

        App::get('logger')->add($mensaje);

    }

    $imagenes = $imgRepository->findAll();
} catch (QueryException $queryException) {
    $erroresValidacion[] = $queryException->getMessage();
} catch (FileException $fileException) {
    $erroresValidacion[] = $fileException->getMessage();
} catch (AppException $appException) {
    $erroresValidacion[] = $appException->getMessage();
}
