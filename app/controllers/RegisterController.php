<?php

namespace proyecto\app\controllers;

use PDOException;
use proyecto\app\entity\UsuarioEntity;
use proyecto\app\exception\AppException;
use proyecto\app\exception\QueryException;
use proyecto\app\exception\ValidationException;
use proyecto\app\repository\UsuarioEntityRepository;
use proyecto\core\App;
use proyecto\core\helpers\FlashMessage;
use proyecto\core\Response;

class RegisterController
{

    public function register()
    {
        $errores = FlashMessage::get('register-error', []);

        Response::renderView('register', 'layout', compact('errores'));
    }

    /**
     * @throws ValidationException
     */
    public function check_register()
    {

        try {

            $userRepository = App::getRepository(UsuarioEntityRepository::class);

            if($_SERVER["REQUEST_METHOD"] === 'POST'){

                $email = trim(htmlspecialchars($_POST['email']));
                $password = trim(htmlspecialchars($_POST['password']));
                $nombre = trim(htmlspecialchars($_POST['nombre']));
                $apellidos = trim(htmlspecialchars($_POST['apellidos']));
                $telefono = trim(htmlspecialchars($_POST['telefono']));
                $direccion = trim(htmlspecialchars($_POST['direccion']));

                if (empty($email) || empty($password) || empty($nombre) || empty($apellidos)) {
                    if (empty($email) && empty($password) && empty($nombre) && empty($apellidos)) {
                        throw new ValidationException('Debes introducir todos los campos necesarios');
                    } else {
                        if (empty($nombre)) {
                            throw new ValidationException('Debes introducir un nombre');
                        }
                        if (empty($apellidos)) {
                            throw new ValidationException('Debes introducir tu apellido');
                        }
                        if (empty($email)) {
                            throw new ValidationException('Debes introducir un correo');
                        }
                        if(empty($password)){
                            throw new ValidationException('Debes introducir un password');
                        }
                    }
                }
                if (!empty($email)) {
                    if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                        throw new ValidationException('Debes introducir un correo válido');
                    }

                    $users = $userRepository->findAll();
                    $correos = [];
                    foreach($users as $user){
                        $correos[] = $user->getEmail();
                    }

                    if(in_array($email, $correos)){
                        throw new ValidationException('Este correo ya está registrado');
                    } else {
                        if(!empty($password) || !empty($nombre) || !empty($apellidos)){
                            $userEntity = new UsuarioEntity($email, password_hash($password, PASSWORD_BCRYPT), $nombre, $apellidos, $telefono, $direccion);
                            $userRepository->save($userEntity);

                            $mensaje = "Se ha registrado un nuevo usuario a la web el dia " . date('Y-m-d');
                            App::get('logger')->add($mensaje);

                            Response::renderView('register', 'layout', compact('userEntity', 'userRepository'));

                            App::get('router')->redirect('');
                        }
                    }
                }

            }

        } catch (QueryException $queryException) {
            FlashMessage::set('register-error', [$queryException->getMessage()]);

            App::get('router')->redirect('register');
        } catch (AppException $appException) {
            FlashMessage::set('register-error', [$appException->getMessage()]);

            App::get('router')->redirect('register');
        } catch (PDOException $pdoException){
            FlashMessage::set('register-error', [$pdoException->getMessage()]);

            App::get('router')->redirect('register');
        } catch (ValidationException $validationException){
            FlashMessage::set('register-error', [$validationException->getMessage()]);

            App::get('router')->redirect('register');
        }

    }

}