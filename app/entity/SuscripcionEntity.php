<?php
namespace proyecto\app\entity;

use proyecto\core\database\IEntity;

class SuscripcionEntity implements IEntity
{

    /**
     * @var int
     */
    private $id_usuario;

    /**
     * @var int
     */
    private $id_plan_entreno;

    /**
     * @var \DateTime
     */
    private $fecha_inscripcion;

    /**
     * @param int $id_usuario
     * @param int $id_plan_entreno
     */
    public function __construct(int $id_usuario=0, int $id_plan_entreno=0)
    {
        $this->id_usuario = $id_usuario;
        $this->id_plan_entreno = $id_plan_entreno;
        $this->fecha_inscripcion = Date('Y-m-d');
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return 'suscripcion';

    }

    /**
     * @return int
     */
    public function getIdUsuario(): int
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return SuscripcionEntity
     */
    public function setIdUsuario(int $id_usuario): SuscripcionEntity
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdPlanEntreno(): int
    {
        return $this->id_plan_entreno;
    }

    /**
     * @param int $id_plan_entreno
     * @return SuscripcionEntity
     */
    public function setIdPlanEntreno(int $id_plan_entreno): SuscripcionEntity
    {
        $this->id_plan_entreno = $id_plan_entreno;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFechaInscripcion(): \DateTime|string
    {
        return $this->fecha_inscripcion;
    }

    /**
     * @param \DateTime $fecha_inscripcion
     * @return SuscripcionEntity
     */
    public function setFechaInscripcion(\DateTime|string $fecha_inscripcion): SuscripcionEntity
    {
        $this->fecha_inscripcion = $fecha_inscripcion;
        return $this;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id_usuario' => $this->getIdUsuario(),
            'id_plan_entreno' => $this->getIdPlanEntreno(),
            'fecha_inscripcion' => $this->getFechaInscripcion()
        ];
    }
}