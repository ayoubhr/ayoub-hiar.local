<?php
namespace proyecto\app\entity;

use proyecto\core\database\IEntity;

class CategoriaEntity implements IEntity
{

    /**
     * @var string
     */
    private $tipo;

    /**
     * @param string $tipo
     */
    public function __construct(string $tipo="")
    {
        $this->tipo = $tipo;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getTipo();
    }

    /**
     * @return string
     */
    public function getTipo() : string
    {
        return $this->tipo;
    }

    /**
     * @param string $tipo
     * @return CategoriaEntity
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'tipo' => $this->getTipo()
        ];
    }
}