<?php
namespace proyecto\app\entity;

use DateTime;
use proyecto\core\database\IEntity;

/**
 * @method getLoginDate()
 */
class ComentarioEntity implements IEntity
{
    /**
     * @var int
     */
    private $usuario_id;

    /**
     * @var string
     */
    private $texto;

    /**
     * @var int
     */
    private $rutina_id;

    /**
     * @var DateTime
     */
    private $date;


    /**
     * @param int $usuario_id
     * @param string $texto
     * @param int $rutina_id
     */
    public function __construct(int $usuario_id=0, string $texto="", int $rutina_id=0)
    {
        $this->usuario_id = $usuario_id;
        $this->texto = $texto;
        $this->rutina_id = $rutina_id;
        $this->date = date("Y-m-d H:i:s");
    }


    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getTexto();
    }

    /**
     * @return int
     */
    public function getUsuarioId(): int
    {
        return $this->usuario_id;
    }

    /**
     * @param int $usuario_id
     * @return ComentarioEntity
     */
    public function setUsuarioId(int $usuario_id): ComentarioEntity
    {
        $this->usuario_id = $usuario_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTexto(): string
    {
        return $this->texto;
    }

    /**
     * @param string $texto
     * @return ComentarioEntity
     */
    public function setTexto(string $texto): ComentarioEntity
    {
        $this->texto = $texto;
        return $this;
    }

    /**
     * @return int
     */
    public function getRutinaId(): int
    {
        return $this->rutina_id;
    }

    /**
     * @param int $rutina_id
     * @return ComentarioEntity
     */
    public function setRutinaId(int $rutina_id): ComentarioEntity
    {
        $this->rutina_id = $rutina_id;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime|string
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     * @return ComentarioEntity
     */
    public function setDate(DateTime|string $date): ComentarioEntity
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'usuario_id' => $this->getUsuarioId(),
            'texto' => $this->getTexto(),
            'rutina_id' => $this->getRutinaId(),
            'date' => $this->getDate()
        ];
    }
}