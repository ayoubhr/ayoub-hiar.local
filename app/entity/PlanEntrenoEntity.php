<?php
namespace proyecto\app\entity;

use proyecto\core\database\IEntity;

class PlanEntrenoEntity implements IEntity
{

    /**
     * @var string
     */
    private $imagen;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $ejercicio_uno;

    /**
     * @var string
     */
    private $ejercicio_dos;

    /**
     * @var string
     */
    private $ejercicio_tres;

    /**
     * @var string
     */
    private $ejercicio_cuatro;

    /**
     * @var string
     */
    private $ejercicio_cinco;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var int
     */
    private $usuarioCreador_id;

    /**
     * @var string
     */
    private $tipo;

    /**
     * @param string $imagen
     * @param string $nombre
     * @param string $ejercicio_uno
     * @param string $ejercicio_dos
     * @param string $ejercicio_tres
     * @param string $ejercicio_cuatro
     * @param string $ejercicio_cinco
     * @param string $descripcion
     * @param int $usuarioCreador_id
     * @param string $tipo
     */
    public function __construct(string $imagen ="", string $nombre="", string $ejercicio_uno="", string $ejercicio_dos="",
                                string $ejercicio_tres="", string $ejercicio_cuatro="",
                                string $ejercicio_cinco="", string $descripcion="", int $usuarioCreador_id=0,
                                string $tipo="")
    {
        $this->imagen = $imagen;
        $this->nombre = $nombre;
        $this->ejercicio_uno = $ejercicio_uno;
        $this->ejercicio_dos = $ejercicio_dos;
        $this->ejercicio_tres = $ejercicio_tres;
        $this->ejercicio_cuatro = $ejercicio_cuatro;
        $this->ejercicio_cinco = $ejercicio_cinco;
        $this->descripcion = $descripcion;
        $this->usuarioCreador_id = $usuarioCreador_id;
        $this->tipo = $tipo;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return 'plan_entreno';
    }

    /**
     * @return string
     */
    public function getImagen(): string
    {
        return $this->imagen;
    }

    /**
     * @param string $imagen
     * @return PlanEntrenoEntity
     */
    public function setImagen(string $imagen): PlanEntrenoEntity
    {
        $this->imagen = $imagen;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return PlanEntrenoEntity
     */
    public function setNombre(string $nombre): PlanEntrenoEntity
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getEjercicioUno(): string
    {
        return $this->ejercicio_uno;
    }

    /**
     * @param string $ejercicio_uno
     * @return PlanEntrenoEntity
     */
    public function setEjercicioUno(string $ejercicio_uno): PlanEntrenoEntity
    {
        $this->ejercicio_uno = $ejercicio_uno;
        return $this;
    }

    /**
     * @return string
     */
    public function getEjercicioDos(): string
    {
        return $this->ejercicio_dos;
    }

    /**
     * @param string $ejercicio_dos
     * @return PlanEntrenoEntity
     */
    public function setEjercicioDos(string $ejercicio_dos): PlanEntrenoEntity
    {
        $this->ejercicio_dos = $ejercicio_dos;
        return $this;
    }

    /**
     * @return string
     */
    public function getEjercicioTres(): string
    {
        return $this->ejercicio_tres;
    }

    /**
     * @param string $ejercicio_tres
     * @return PlanEntrenoEntity
     */
    public function setEjercicioTres(string $ejercicio_tres): PlanEntrenoEntity
    {
        $this->ejercicio_tres = $ejercicio_tres;
        return $this;
    }

    /**
     * @return string
     */
    public function getEjercicioCuatro(): string
    {
        return $this->ejercicio_cuatro;
    }

    /**
     * @param string $ejercicio_cuatro
     * @return PlanEntrenoEntity
     */
    public function setEjercicioCuatro(string $ejercicio_cuatro): PlanEntrenoEntity
    {
        $this->ejercicio_cuatro = $ejercicio_cuatro;
        return $this;
    }

    /**
     * @return string
     */
    public function getEjercicioCinco(): string
    {
        return $this->ejercicio_cinco;
    }

    /**
     * @param string $ejercicio_cinco
     * @return PlanEntrenoEntity
     */
    public function setEjercicioCinco(string $ejercicio_cinco): PlanEntrenoEntity
    {
        $this->ejercicio_cinco = $ejercicio_cinco;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescripcion(): string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     * @return PlanEntrenoEntity
     */
    public function setDescripcion(string $descripcion): PlanEntrenoEntity
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return int
     */
    public function getUsuarioCreadorId(): int
    {
        return $this->usuarioCreador_id;
    }

    /**
     * @param int $usuarioCreador_id
     * @return PlanEntrenoEntity
     */
    public function setUsuarioCreadorId(int $usuarioCreador_id): PlanEntrenoEntity
    {
        $this->usuarioCreador_id = $usuarioCreador_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipo(): string
    {
        return $this->tipo;
    }

    /**
     * @param string $tipo
     * @return PlanEntrenoEntity
     */
    public function setTipo(string $tipo): PlanEntrenoEntity
    {
        $this->tipo = $tipo;
        return $this;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'imagen' => $this->getImagen(),
            'nombre' => $this->getNombre(),
            'ejercicio_uno' => $this->getEjercicioUno(),
            'ejercicio_dos' => $this->getEjercicioDos(),
            'ejercicio_tres' => $this->getEjercicioTres(),
            'ejercicio_cuatro' => $this->getEjercicioCuatro(),
            'ejercicio_cinco' => $this->getEjercicioCinco(),
            'descripcion' => $this->getDescripcion(),
            'usuarioCreador_id' => $this->getUsuarioCreadorId(),
            'tipo' => $this->getTipo()
        ];
    }
}