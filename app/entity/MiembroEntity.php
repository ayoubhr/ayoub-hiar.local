<?php
namespace proyecto\app\entity;

use proyecto\core\database\IEntity;

class MiembroEntity implements IEntity
{
    /**
     * @var int
     */
    private $usuario_id;

    /**
     * @var int
     */
    private $cursos_suscritos;

    /**
     * @param int $usuario_id
     * @param int $cursos_suscritos
     */
    public function __construct(int $usuario_id, int $cursos_suscritos)
    {
        $this->usuario_id = $usuario_id;
        $this->cursos_suscritos = $cursos_suscritos;
    }

    /**
     * @return int
     */
    public function getUsuarioId(): int
    {
        return $this->usuario_id;
    }

    /**
     * @param int $usuario_id
     * @return MiembroEntity
     */
    public function setUsuarioId(int $usuario_id): MiembroEntity
    {
        $this->usuario_id = $usuario_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCursosSuscritos(): int
    {
        return $this->cursos_suscritos;
    }

    /**
     * @param int $cursos_suscritos
     * @return MiembroEntity
     */
    public function setCursosSuscritos(int $cursos_suscritos): MiembroEntity
    {
        $this->cursos_suscritos = $cursos_suscritos;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'usuario_id' => $this->getUsuarioId(),
            'cursos_suscritos' => $this->getCursosSuscritos()
        ];
    }
}