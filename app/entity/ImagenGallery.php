<?php
namespace proyecto\app\entity;

use proyecto\core\database\IEntity;

class ImagenGallery implements IEntity
{
    //images/web/web
    //images/gallery/gallery
    const RUTA_IMAGEN_WEB = __DIR__ . "/../../public/images/web/web";
    const RUTA_IMAGEN_GALLERY = __DIR__ . "/../../public/images/gallery/gallery";

    /**
     * @var string
     */
    private $nombre;
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @param string $nombre
     * @param string $descripcion
     */
    public function __construct(string $nombre = "", string $descripcion = "")
    {
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getDescripcion();
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return ImagenGallery
     */
    public function setNombre(string $nombre): ImagenGallery
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescripcion(): string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     * @return ImagenGallery
     */
    public function setDescripcion(string $descripcion): ImagenGallery
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    public function getUrlWeb():string
    {
        return self::RUTA_IMAGEN_WEB . $this->getNombre();
    }

    public function getUrlGallery():string
    {
        return self::RUTA_IMAGEN_GALLERY . $this->getNombre();
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'nombre' => $this->getNombre(),
            'descripcion' => $this->getDescripcion()
        ];
    }
}