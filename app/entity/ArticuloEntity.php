<?php

namespace proyecto\app\entity;

use DateTime;
use proyecto\core\database\IEntity;

class ArticuloEntity implements IEntity
{

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var int
     */
    private $precio;

    /**
     * @var DateTime|string
     */
    private $fecha_caducidad;

    /**
     * @var string
     */
    private $imagen;

    /**
     * @var int
     */
    private $id_usuario;

    public function __construct(string $nombre="", int $precio=0, string $imagen="", int $id_usuario=0)
    {
        $this->nombre = $nombre;
        $this->precio = $precio;
        $this->fecha_caducidad = date('Y-m-d');
        $this->imagen = $imagen;
        $this->id_usuario = $id_usuario;
    }

    /**
     * string
     */
    public function __toString()
    {
        return 'articulo';
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return ArticuloEntity
     */
    public function setNombre(string $nombre): ArticuloEntity
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrecio(): int
    {
        return $this->precio;
    }

    /**
     * @param int $precio
     * @return ArticuloEntity
     */
    public function setPrecio(int $precio): ArticuloEntity
    {
        $this->precio = $precio;
        return $this;
    }

    /**
     * @return DateTime|string
     */
    public function getFechaCaducidad(): DateTime|string
    {
        return $this->fecha_caducidad;
    }

    /**
     * @param DateTime|string $fecha_caducidad
     * @return ArticuloEntity
     */
    public function setFechaCaducidad(DateTime|string $fecha_caducidad): ArticuloEntity
    {
        $this->fecha_caducidad = $fecha_caducidad;
        return $this;
    }

    /**
     * @return string
     */
    public function getImagen(): string
    {
        return $this->imagen;
    }

    /**
     * @param string $imagen
     * @return ArticuloEntity
     */
    public function setImagen(string $imagen): ArticuloEntity
    {
        $this->imagen = $imagen;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdUsuario(): int
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return ArticuloEntity
     */
    public function setIdUsuario(int $id_usuario): ArticuloEntity
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'nombre' => $this->getNombre(),
            'precio' => $this->getPrecio(),
            'fecha_caducidad' => $this->getFechaCaducidad(),
            'imagen' => $this->getImagen(),
            'id_usuario' => $this->getIdUsuario()
        ];
    }
}