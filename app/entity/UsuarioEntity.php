<?php
namespace proyecto\app\entity;

use DateTime;
use proyecto\core\database\IEntity;

class UsuarioEntity implements IEntity
{

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $apellidos;

    /**
     * @var string
     */
    private $telefono;

    /**
     * @var string
     */
    private $direccion;

    /**
     * @var DateTime
     */
    private $fecha_alta;

    /**
     * @var string
     */
    private $rol;

    /**
     * @param string $email
     * @param string $password
     * @param string $nombre
     * @param string $apellidos
     * @param string $telefono
     * @param string $direccion
     * @param DateTime $fecha_alta
     */
    public function __construct(string $email="", string $password="", string $nombre="", string $apellidos="", string $telefono="", string $direccion="")
    {
        $this->email = $email;
        $this->password = $password;
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->fecha_alta = date('Y-m-d');
        $this->telefono = $telefono;
        $this->direccion = $direccion;
        $this->rol = 'ROLE_USER';
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return 'usuario';
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return UsuarioEntity
     */
    public function setEmail(string $email): UsuarioEntity
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return UsuarioEntity
     */
    public function setPassword(string $password): UsuarioEntity
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return UsuarioEntity
     */
    public function setNombre(string $nombre): UsuarioEntity
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getApellidos(): string
    {
        return $this->apellidos;
    }

    /**
     * @param string $apellidos
     * @return UsuarioEntity
     */
    public function setApellidos(string $apellidos): UsuarioEntity
    {
        $this->apellidos = $apellidos;
        return $this;
    }

    /**
     * @return DateTime|string
     */
    public function getFechaAlta(): DateTime|string
    {
        return $this->fecha_alta;
    }

    /**
     * @param DateTime|string $fecha_alta
     * @return $this
     */
    public function setFechaAlta(DateTime|string $fecha_alta): UsuarioEntity
    {
        $this->fecha_alta = $fecha_alta;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelefono(): string
    {
        return $this->telefono;
    }

    /**
     * @param string $telefono
     * @return UsuarioEntity
     */
    public function setTelefono(string $telefono): UsuarioEntity
    {
        $this->telefono = $telefono;
        return $this;
    }

    /**
     * @return string
     */
    public function getDireccion(): string
    {
        return $this->direccion;
    }

    /**
     * @param string $direccion
     * @return UsuarioEntity
     */
    public function setDireccion(string $direccion): UsuarioEntity
    {
        $this->direccion = $direccion;
        return $this;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getRol(): string
    {
        return $this->rol;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'email' => $this->getEmail(),
            'password' => $this->getPassword(),
            'nombre' => $this->getNombre(),
            'apellidos' => $this->getApellidos(),
            'fecha_alta' => $this->getFechaAlta(),
            'telefono' => $this->getTelefono(),
            'direccion' => $this->getDireccion(),
            'rol' => $this->getRol()
        ];
    }
}