<?php
namespace proyecto\app\entity;

use proyecto\core\database\IEntity;

class ContactEntity implements IEntity
{
    /**
     * @var string
     */
    private $fullName;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $message;

    /**
     * @param string $fullName
     * @param string $phone
     * @param string $email
     * @param string $message
     */
    public function __construct(string $fullName="", string $message="", string $email="", string $phone="")
    {
        $this->fullName = $fullName;
        $this->message = $message;
        $this->email = $email;
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getMessage();
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     * @return ContactEntity
     */
    public function setFullName(string $fullName): ContactEntity
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return ContactEntity
     */
    public function setPhone(string $phone): ContactEntity
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return ContactEntity
     */
    public function setEmail(string $email): ContactEntity
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return ContactEntity
     */
    public function setMessage(string $message): ContactEntity
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'fullName' => $this->getFullName(),
            'phone' => $this->getPhone(),
            'email' => $this->getEmail(),
            'message' => $this->getMessage()
        ];
    }
}