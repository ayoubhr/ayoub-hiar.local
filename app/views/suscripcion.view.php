<form action="confirma_suscripcion" method="POST" class="p-5 bg-white">
        <div class="col-md-12">
            <label class="font-weight-bold" for="suscripcion"><?php echo 'Usuario logueado: ' . $user ?></label>
        </div>
    <div style="margin-left: 15px">
        <p>Desea suscribirse a la rutina?</p>
        <label for="si">Si</label>
        <input type="radio" id="si" name="eleccion" value="si" class="btn btn-primary text-white px-4 py-2" style="margin-left: 15px;">
        <label for="no">No</label>
        <input type="radio" id="no" name="eleccion" value="no" class="btn btn-primary text-white px-4 py-2" style="margin-left: 15px;">
    </div>
    <div class="row form-group">
        <div class="col-md-12">
            <input type="submit" value="suscribeme!" class="btn btn-primary text-white px-4 py-2">
        </div>
    </div>
</form>
