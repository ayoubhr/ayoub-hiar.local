<?php include __DIR__ . '/partials/inicio_doc.partial.php'; ?>
  <body style="background-image: url('../../public/images/bg.jpg');">

  <div class="site-wrap">
    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->


    <div class="site-navbar-wrap js-site-navbar bg-white">

      <div class="container">
        <div class="site-navbar bg-light">
          <div class="py-1">
            <div class="row align-items-center">
              <div class="col-2">
                <h2 class="mb-0 site-logo"><a href="/">Calis<strong>thenics</strong></a></h2>
              </div>
              <div class="col-10">
                  <?php include __DIR__ . '/partials/nav.partial.php'; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

      <?= $mainContent ?>

      <?php include __DIR__ . '/partials/footer.partial.php'; ?>
  </div>

<?php include __DIR__ . '/partials/fin_doc.partial.php'; ?>
