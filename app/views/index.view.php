

    <!-- <div style="height: 113px;"></div> -->
    <div class="slide-one-item home-slider owl-carousel">
      
      <div class="site-blocks-cover inner-page" style="background-image: url(<?= '/images/hero_b1_1.jpg' ?>);" data-aos="fade" data-stellar-background-ratio="0.5">
        <!-- <div class="container"> -->
          <div class="row align-items-center justify-content-center">
            <div class="col-md-7 text-center" data-aos="fade">
              <h1>Welcome To Calisthenics Nation</h1>
              <span class="caption d-block text-white">Find The Healthy Way</span>
            </div>
          </div>
        <!-- </div> -->
      </div>  

      <div class="site-blocks-cover inner-page" style="background-image: url(<?= '/images/hero_bg_2.jpg' ?>);" data-aos="fade" data-stellar-background-ratio="0.5">
        <!-- <div class="container"> -->
          <div class="row align-items-center justify-content-center">
            <div class="col-md-7 text-center" data-aos="fade">
              <h1>Optimize Your Health</h1>
              <span class="caption d-block text-white">Effective Program</span>
            </div>
          </div>
        <!-- </div> -->
      </div> 
    </div>

    <div class="site-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="site-section-heading text-center">Popular Program</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 block-13 nav-direction-white">
                    <div class="nonloop-block-13 owl-carousel">
                        <!-- EL contenedor de imagenes va de aqui -->
                        <?php if (!empty($arrayElementos)) : ?>
                            <?php for ($i = 0; $i < count($arrayElementos); $i++) : ?>
                                <?php if ($i === 6): ?>
                                    <?php break; ?>
                                <?php endif; ?>
                                    <div class="media-image">
                                        <?php echo '<img src="/images/gallery/gallery' . $arrayElementos[$i]->getImagen() . '"alt=image"' . 'class="img-fluid"' . '"/>' ?>
                                        <div class="media-image-body">
                                            <h2><?= $arrayElementos[$i]->getNombre() ?></h2>
                                            <p>Especialidad: <?= $arrayElementos[$i]->getTipo() ?></p>
                                            <p>
                                                Autor: <?= $usuariosPrincipales[$i]->getNombre() . ' ' . $usuariosPrincipales[$i]->getApellidos() ?></p>
                                            <p>
                                                <a href="<?= 'detalles_rutina/' . $arrayElementos[$i]->getId() ?>" class="btn btn-primary text-white px-4">
                                                    <span class="caption">Learn More</span>
                                                </a>
                                            </p>
                                        </div>

                                    </div>
                            <?php endfor; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if (!empty($arrayRestoElementos)) : ?>
        <div class="site-section">
            <div class="block-13 " style="display: flex">
                <?php for ($i = 0; $i < 3; $i++) : ?>
                    <?php if (!array_key_exists($i, $arrayRestoElementos)): ?>
                        <?php break; ?>
                    <?php else : ?>
                        <div class="media-image" style="margin: 7px;">
                            <?php echo '<img src="/images/gallery/gallery' . $arrayRestoElementos[$i]->getImagen() . '"alt=image"' . 'class="img-fluid"' . '"/>' ?>
                            <div class="media-image-body">
                                <h2><?= $arrayRestoElementos[$i]->getNombre() ?></h2>
                                <p>Especialidad: <?= $arrayRestoElementos[$i]->getTipo() ?></p>
                                <p>
                                    Autor: <?= $restoUsuarios[$i]->getNombre() . ' ' . $restoUsuarios[$i]->getApellidos() ?></p>
                                <p><a href="<?= '/detalles_rutina/' . $arrayRestoElementos[$i]->getId() ?>"
                                      class="btn btn-primary text-white px-4"><span
                                                class="caption">Learn More</span></a></p>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>
        </div>
    <?php endif; ?>

    <?php $nuevoArray = array_slice($arrayRestoElementos, 3)?>

    <?php if (!empty($nuevoArray)) : ?>
        <div class="site-section">
            <div class="block-13 " style="display: flex">
                <?php for ($i = 0; $i < count($nuevoArray); $i++) : ?>
                    <?php if (!array_key_exists($i, $nuevoArray)): ?>
                        <?php break; ?>
                    <?php else : ?>
                        <div class="media-image" style="margin: 7px;">
                            <?php echo '<img src="/images/gallery/gallery' . $nuevoArray[$i]->getImagen() . '"alt=image"' . 'class="img-fluid"' . '"/>' ?>
                            <div class="media-image-body">
                                <h2><?= $nuevoArray[$i]->getNombre() ?></h2>
                                <p>Especialidad: <?= $nuevoArray[$i]->getTipo() ?></p>
                                <p>
                                    Autor: <?= $restoUsuarios[$i]->getNombre() . ' ' . $restoUsuarios[$i]->getApellidos() ?></p>
                                <p><a href="<?= '/detalles_rutina/' . $nuevoArray[$i]->getId() ?>"
                                      class="btn btn-primary text-white px-4"><span
                                                class="caption">Learn More</span></a></p>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($i === 2) : ?>
                        <?php break; ?>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>
        </div>
    <?php endif; ?>

    <?php $nuevoArraySegundo = array_slice($nuevoArray, 3)?>

    <?php if (!empty($nuevoArraySegundo)) : ?>
        <div class="site-section">
            <div class="block-13 " style="display: flex">
                <?php for ($i = 0; $i < count($nuevoArraySegundo); $i++) : ?>
                    <?php if (!array_key_exists($i, $nuevoArraySegundo)): ?>
                        <?php break; ?>
                    <?php else : ?>
                        <div class="media-image" style="margin: 7px;">
                            <?php echo '<img src="/images/gallery/gallery' . $nuevoArraySegundo[$i]->getImagen() . '"alt=image"' . 'class="img-fluid"' . '"/>' ?>
                            <div class="media-image-body">
                                <h2><?= $nuevoArraySegundo[$i]->getNombre() ?></h2>
                                <p>Especialidad: <?= $nuevoArraySegundo[$i]->getTipo() ?></p>
                                <p>
                                    Autor: <?= $restoUsuarios[$i]->getNombre() . ' ' . $restoUsuarios[$i]->getApellidos() ?></p>
                                <p><a href="<?= '/detalles_rutina/' . $nuevoArraySegundo[$i]->getId() ?>"
                                      class="btn btn-primary text-white px-4"><span
                                                class="caption">Learn More</span></a></p>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($i === 2) : ?>
                        <?php break; ?>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>
        </div>
    <?php endif; ?>

    <?php $nuevoArrayTercero = array_slice($nuevoArraySegundo, 3)?>

    <?php if (!empty($nuevoArrayTercero)) : ?>
        <div class="site-section">
            <div class="block-13 " style="display: flex">
                <?php for ($i = 0; $i < count($nuevoArrayTercero); $i++) : ?>
                    <?php if (!array_key_exists($i, $nuevoArrayTercero)): ?>
                        <?php break; ?>
                    <?php else : ?>
                        <div class="media-image" style="margin: 7px;">
                            <?php echo '<img src="/images/gallery/gallery' . $nuevoArrayTercero[$i]->getImagen() . '"alt=image"' . 'class="img-fluid"' . '"/>' ?>
                            <div class="media-image-body">
                                <h2><?= $nuevoArrayTercero[$i]->getNombre() ?></h2>
                                <p>Especialidad: <?= $nuevoArrayTercero[$i]->getTipo() ?></p>
                                <p>
                                    Autor: <?= $restoUsuarios[$i]->getNombre() . ' ' . $restoUsuarios[$i]->getApellidos() ?></p>
                                <p><a href="<?= '/detalles_rutina/' . $nuevoArrayTercero[$i]->getId() ?>"
                                      class="btn btn-primary text-white px-4"><span
                                                class="caption">Learn More</span></a></p>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($i === 2) : ?>
                        <?php break; ?>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>
        </div>
    <?php endif; ?>





