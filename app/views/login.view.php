
    <div class="py-5 bg-light">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-lg-8 mb-5">

                    <h1>Login</h1>
                    <?php include __DIR__ . '/partials/errors.partial.php'; ?>

                    <form action="<?='check-login'?>" method="POST" class="p-5 bg-white">

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="email">email</label>
                                <input type="email" id="email" name="email" class="form-control"
                                       value="<?php $email ?? '' ?>">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="password">password</label>
                                <input type="password" id="password" name="password" class="form-control">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <input type="submit" value="Login" class="btn btn-primary text-white px-4 py-2">
                            </div>
                        </div>

                    </form>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <a href="/register">Register</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


