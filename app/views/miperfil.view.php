<?php include __DIR__ . '/partials/inicio_doc.partial.php'; ?>

<body style="background-image: url('../../public/images/bg.jpg');">

<div class="site-wrap">
    <div class="py-5 bg-light">
        <div class="container">
            <h3>Secciones en mi perfil:</h3>
            <div class="row">

                <div class="col-md-12 col-lg-8 mb-5">
                    <p><a href="/mis_suscripciones">Mis Suscripciones</a></p>
                    <p><a href="/mis_rutinas">Mis rutinas</a></p>
                    <p><a href="/actividad_usuario">Mi actividad</a></p>
                    <?php if(!is_null($app['user']) && $app['user']->getId() === 11):?>
                        <p><a href="/administrador">Panel de administración</a></p>
                    <?php endif; ?>
                    <h4>Cambio de password:</h4>
                    <form action="/cambioPass" method="POST">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password">

                        <label for="newPassword">Password nuevo</label>
                        <input type="password" name="newPassword" id="newPassword">

                        <div class="row form-group">
                            <div class="col-md-12">
                                <input type="submit" value="actualiza" class="btn btn-primary text-white px-4 py-2">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php include __DIR__ . '/partials/fin_doc.partial.php'; ?>


