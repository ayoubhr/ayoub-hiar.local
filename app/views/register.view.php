<div class="site-wrap">
    <div class="py-5 bg-light">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-lg-8 mb-5">

                    <h1>Registro</h1>
                    <?php include __DIR__ . '/partials/errors.partial.php'; ?>
                    <form action="<?= 'register' ?>" method="POST" class="p-5 bg-white">

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="email">email</label>
                                <input type="email" id="email" name="email" class="form-control" placeholder="email"
                                       value="">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="password">password</label>
                                <input type="password" id="password" name="password" class="form-control" placeholder="password"
                                       value="">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="nombre">nombre</label>
                                <input type="text" id="nombre" name="nombre" class="form-control" placeholder="nombre"
                                       value="">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="apellidos">apellidos</label>
                                <input type="text" id="apellidos" name="apellidos" class="form-control" placeholder="apellidos"
                                       value="">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="telefono">telefono</label>
                                <input type="text" id="telefono" name="telefono" class="form-control" placeholder="telefono"
                                       value="">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="direccion">direccion</label>
                                <input type="text" id="direccion" name="direccion" class="form-control" placeholder="direccion"
                                       value="">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <input type="submit" value="Register" class="btn btn-primary text-white px-4 py-2">
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
