<form action="<?= '/confirm_delete/' . $id ?>" method="POST" class="p-5 bg-white">
    <div class="col-md-12">
        <label class="font-weight-bold" for="usuario">Action admin delete</label>
    </div>
    <div style="margin-left: 15px">
        <p><?= 'Seguro que desea eliminar al usuario ' . '<b>' . $user->getNombre() . ' ' . $user->getApellidos() . '</b>' . '?' ?></p>
        <label for="si">Si</label>
        <input type="radio" id="si" name="eleccion" value="si" class="btn btn-primary text-white px-4 py-2" style="margin-left: 15px;">
        <label for="no">No</label>
        <input type="radio" id="no" name="eleccion" value="no" class="btn btn-primary text-white px-4 py-2" style="margin-left: 15px;">
    </div>
    <div class="row form-group">
        <div class="col-md-12">
            <input type="submit" value="Eliminar" class="btn btn-primary text-white px-4 py-2">
        </div>
    </div>
</form>