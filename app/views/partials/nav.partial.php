<nav class="site-navigation text-right" role="navigation">
    <div class="container">
        <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="/" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

        <ul class="site-menu js-clone-nav d-none d-lg-block">
            <?php if(!is_null($app['user']) && $app['user']->getId() === 11):?>
                <li><a href="/administrador">AdminPanel</a></li>
            <?php endif; ?>
            <li><a href="/about">About</a></li>
            <li><a href="/contact">Contact</a></li>
            <?php if(is_null($app['user'])) : ?>
                <li><a href="/login">Login</a></li>
                <li><a href="/register">Register</a></li>
            <?php else: ?>
                <li><a href="/miperfil">Mi Perfil</a></li>
                <li><a href="/crear_rutina">Crear rutina</a></li>
                <li><a href="/logout"><i class="fa fa-sign-out" /><?= $app['user']->getNombre() ?> - Logout</a></li>
            <?php endif; ?>
            <li><a href="<?= (is_null(($app['user']))) ? '/login' : '/articulos'?>">Articulos</a></li>
        </ul>
    </div>
</nav>
