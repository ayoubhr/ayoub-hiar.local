<form action="confirma_comentario" method="POST" class="p-5 bg-white">
    <div>
        <div class="col-md-12">
            <label class="font-weight-bold" for="comentario"><?php echo 'Usuario logueado: ' . $user ?></label>
            <textarea name="comentario" id="comentario" cols="30" rows="5" class="form-control"
                      placeholder="comenta.."></textarea>
        </div>
        <input type="submit" value="comenta" class="btn btn-primary text-white px-4 py-2" style="margin-left: 15px;"/>
    </div>
</form>
