<?php include __DIR__ . '/partials/inicio_doc.partial.php'; ?>

<body style="background-image: url('../../public/images/bg.jpg');">

<div class="site-wrap">
    <div class="py-5 bg-light">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-lg-8 mb-5">
                    <h3>Mis Suscripciones:</h3>
                    <div class="col-md-12 block-13 nav-direction-white">
                        <div class="nonloop-block-13 owl-carousel">
                            <!-- EL contenedor de imagenes va de aqui -->
                            <?php if (!empty($arrayElementos)) : ?>
                                <?php for ($i = 0; $i < count($arrayElementos); $i++) : ?>
                                    <?php if ($i === 6): ?>
                                        <?php break; ?>
                                    <?php endif; ?>
                                    <div class="media-image">
                                        <?php echo '<img src="/images/gallery/gallery' . $arrayElementos[$i]->getImagen() . '"alt=image"' . 'class="img-fluid"' . '"/>' ?>
                                        <div class="media-image-body">
                                            <h2><?= $arrayElementos[$i]->getNombre() ?></h2>
                                            <p>Especialidad: <?= $arrayElementos[$i]->getTipo() ?></p>

                                            <p>
                                                <a href="<?= 'detalles_rutina/' . $arrayElementos[$i]->getId() ?>" class="btn btn-primary text-white px-4">
                                                    <span class="caption">Learn More</span>
                                                </a>
                                            </p>
                                        </div>

                                    </div>
                                <?php endfor; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include __DIR__ . '/partials/fin_doc.partial.php'; ?>


