
<div class="site-wrap">
    <div class="py-5 bg-light">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-lg-8 mb-5">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                            <th scope="col">imagen</th>
                            <th scope="col">Fecha Caducidad</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!is_null($articulos)) : ?>
                            <?php for ($i = 0; $i < count($articulos); $i++) : ?>

                                <tr>
                                    <th scope="row"><?= $i + 1 ?></th>
                                    <td><?= $articulos[$i]->getNombre() ?></td>
                                    <td><?= $articulos[$i]->getPrecio() ?></td>
                                    <td><?php echo '<img style="max-width:100px; max-height: 100px;" src="/images/gallery/gallery' . $articulos[$i]->getImagen() . '"alt=image"' . 'class="img-fluid"' . '"/>' ?>
                                    </td>
                                    <td><?= $articulos[$i]->getFechaCaducidad() ?></td>
                                </tr>

                            <?php endfor; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>

                <h3>Formulario para crear un articulo</h3>

                <?php include __DIR__ . '/partials/errors.partial.php'; ?>
                <form action="<?='crear_articulo'?>" method="POST" enctype="multipart/form-data" class="p-5 bg-white">

                    <div class="row form-group">
                        <div class="col-md-12 mb-3 mb-md-0">
                            <label class="font-weight-bold" for="nombre">nombre</label>
                            <input type="text" id="nombre" name="nombre" class="form-control" placeholder="articulo"
                                   value="">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12 mb-3 mb-md-0">
                            <label class="font-weight-bold" for="precio">precio</label>
                            <input type="number" id="precio" name="precio" class="form-control" value="precio"">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12 mb-3 mb-md-0">
                            <label class="font-weight-bold" for="fecha_caducidad">Fecha caducidad</label>
                            <input type="date" id="fecha_caducidad" name="fecha_caducidad" class="form-control" value="fecha">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12 mb-3 mb-md-0">
                            <label class="font-weight-bold" for="imagen">imagen</label>
                            <input type="file" id="imagen" name="imagen" class="form-control" value="search">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <input type="submit" value="Register" class="btn btn-primary text-white px-4 py-2">
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

