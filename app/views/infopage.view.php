<div class="site-wrap">
    <div class="py-5 bg-light">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-lg-8 mb-5">
                    <h3>Datos del usuario <?= $user->getNombre() . ' ' . $user->getApellidos()?></h3>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Suscripciones</th>
                            <th scope="col">Comentarios </th>
                            <th scope="col">Rutinas creadas</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td><?php echo count($arrayElementosSuscripciones) ?></td>
                            <td><?php echo count($arrayElementosComentarios) ?></td>
                            <td><?php echo count($arrayElementosRutinas) ?></td>
                            <td><a href="<?= 'delete_user/' . $user->getId() ?>">Eliminar</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>