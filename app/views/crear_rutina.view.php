<?php include __DIR__ . '/partials/inicio_doc.partial.php'; ?>

<body style="background-image: url('../../public/images/bg.jpg');">

    <div class="py-5 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-8 mb-5">

                    <?php include __DIR__ . '/partials/errors.partial.php'; ?>
                    <form action="<?='crear_rutina'?>" method="POST" enctype="multipart/form-data" class="p-5 bg-white">

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="imagen">imagen</label>
                                <input type="file" id="imagen" name="imagen" class="form-control" value="search">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="nombre">nombre</label>
                                <input type="text" id="email" name="nombre" class="form-control" placeholder="nombre de tu rutina"
                                       value="">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="ejercicio_uno">nombre</label>
                                <input type="text" id="ejercicio_uno" name="ejercicio_uno" class="form-control" placeholder="ejercicio uno"
                                       value="" list="ejercicio_uno">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="ejercicio_dos">nombre</label>
                                <input type="text" id="ejercicio_dos" name="ejercicio_dos" class="form-control" placeholder="ejercicio dos"
                                       value="">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="ejercicio_tres">nombre</label>
                                <input type="text" id="ejercicio_tres" name="ejercicio_tres" class="form-control" placeholder="ejercicio tres"
                                       value="" list="ejercicio_tres">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="ejercicio_cuatro">nombre</label>
                                <input type="text" id="ejercicio_cuatro" name="ejercicio_cuatro" class="form-control" placeholder="ejercicio cuatro"
                                       value="">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="ejercicio_cinco">nombre</label>
                                <input type="text" id="ejercicio_cinco" name="ejercicio_cinco" class="form-control" placeholder="ejercicio cinco"
                                       value="">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="font-weight-bold" for="descripcion">Descripcion</label>
                                <textarea name="descripcion" id="descripcion" cols="30" rows="5" class="form-control"
                                          placeholder="Detalla tu rutina aqui"></textarea>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="tipo">Especialidad</label>
                                <input type="text" id="tipo" name="tipo" class="form-control" placeholder="tipo"
                                       value="">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <input type="submit" value="Register" class="btn btn-primary text-white px-4 py-2">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php include __DIR__ . '/partials/fin_doc.partial.php'; ?>

