

<div class="site-wrap">
    <div class="py-5 bg-light">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-lg-8 mb-5">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Email</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Rol</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php for($i=0; $i<count($usuarios); $i++) : ?>
                            <tr>
                                <th scope="row"><?= $i+1 ?></th>
                                <td><?= $articulos[$i]->getEmail() ?></td>
                                <td><?= $usuarios[$i]->getNombre() ?></td>
                                <td><?= $usuarios[$i]->getApellidos() ?></td>
                                <td><?= $usuarios[$i]->getRol() ?></td>
                                <td><a href="<?= 'info_page/' . $usuarios[$i]->getId() ?>">Info Page</a></td>
                            </tr>
                        <?php endfor; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



