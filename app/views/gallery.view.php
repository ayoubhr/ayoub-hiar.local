<?php include __DIR__ . '/partials/inicio_doc.partial.php'; ?>

<body style="background-image: url('../../public/images/bg.jpg');">

<div class="site-navbar-wrap js-site-navbar bg-white">

    <div class="container">
        <div class="site-navbar bg-light">
            <div class="py-1">
                <div class="row align-items-center">
                    <div class="col-2">
                        <h2 class="mb-0 site-logo"><a href="/">Cross<strong>fits</strong></a></h2>
                    </div>
                    <div class="col-10">
                        <?php include __DIR__ . '/partials/nav.partial.php'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <div class="site-wrap">
    <div class="py-5 bg-light">
      <div class="container">
        <div class="row">

          <div class="col-md-12 col-lg-8 mb-5">

            <?php if($_SERVER['REQUEST_METHOD'] === "POST") : ?>
                <div>
                    <?php if(empty($erroresValidacion)) : ?>
                        <p>Guardada!</p>
                    <?php else : ?>
                        <ul>
                            <?php foreach($erroresValidacion as $error) : ?>
                                <li><?= $error ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <form action="<?= 'gallery' ?>" method="POST" enctype="multipart/form-data" class="p-5 bg-white">

                <div class="row form-group">
                    <div class="col-md-12 mb-3 mb-md-0">
                        <label class="font-weight-bold" for="imagen">imagen</label>
                        <input type="file" id="imagen" name="imagen" class="form-control" placeholder="imagen"
                               value="search">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-12">
                        <label class="font-weight-bold" for="descripcion">Message</label>
                        <textarea name="descripcion" id="descripcion" cols="30" rows="5" class="form-control"
                                  placeholder="Say hello to us"></textarea>
                    </div>
                </div>

                <div class="row form-group">
                <div class="col-md-12">
                  <input type="submit" value="Send Message" class="btn btn-primary text-white px-4 py-2">
                </div>
              </div>

            </form>

          </div>
        </div>
      </div>
    </div>
     <?php include __DIR__ . '/partials/footer.partial.php'; ?>
<?php include __DIR__ . '/partials/fin_doc.partial.php'; ?>
