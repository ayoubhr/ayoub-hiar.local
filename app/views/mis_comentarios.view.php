<?php include __DIR__ . '/partials/inicio_doc.partial.php'; ?>

<body style="background-image: url('../../public/images/bg.jpg');">

<div class="site-wrap">
    <div class="py-5 bg-light">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-lg-8 mb-5">
                    <h3>Mis comentarios [en esta rutina]:</h3>
                    <?php if(!empty($arrayElementos)) : ?>
                        <?php for($i=0; $i<count($arrayElementos); $i++) :?>
                            <?php echo '<p>' . $user->getNombre() . ' ' . $user->getApellidos() . ': '
                                . $arrayElementos[$i]->getTexto() . ' [' . '<a href="' . '/detalles_rutina/' . $id_rutina . '">' . $arrayRutinas[$i] . '</a>' . ']' . '</p>'?>
                        <?php endfor; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <?php include __DIR__ . '/partials/fin_doc.partial.php'; ?>

