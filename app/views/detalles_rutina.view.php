

<div class="site-wrap">
    <div class="py-5 bg-light">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-lg-8 mb-5">
                    <h3>Detalles de la rutina</h3>
                    <div class="block-13 ">
                        <?php echo '<img src="/images/gallery/gallery' . $detallesRutina->getImagen() . '"' . ' ' .'"alt=image"' . ' ' .  'style="max-width: 1000px; max-height: 400px;"' . '/>' ?>
                    </div>
                    <div class="media-image" style="margin-top: 25px; display: grid;">
                        <!-- image tag here -->
                        <div class="media-image-body">
                            <?php echo '<p>Nombre de la rutina: ' . $detallesRutina->getNombre() . ' created by ' . $nombre ?>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">Primero</th>
                                    <th scope="col">Segundo</th>
                                    <th scope="col">Tercero</th>
                                    <th scope="col">Cuarto</th>
                                    <th scope="col">Quinto</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <?php echo '<td>' . $detallesRutina->getEjercicioUno() . '</td>'  ?>
                                    <?php echo '<td>' . $detallesRutina->getEjercicioDos() . '</td>'  ?>
                                    <?php echo '<td>' . $detallesRutina->getEjercicioTres() . '</td>' ?>
                                    <?php echo '<td>' . $detallesRutina->getEjercicioCuatro() . '</td>'?>
                                    <?php echo '<td>' . $detallesRutina->getEjercicioCinco() . '</td>' ?>
                                </tr>
                                </tbody>
                            </table>
                            <?php echo '<p>' . 'Especialidad: ' . $detallesRutina->getTipo() . '</p>'?>
                            <?php echo '<p>' . 'Descripcion: ' . $detallesRutina->getDescripcion() . '</p>'?>
                            <?php if($totalSuscripciones === 0) : ?>
                                <?php echo '<p>' . 'Usuarios suscritos: 0' . '</p>'?>
                            <?php else :?>
                                <?php echo '<p>' . 'Usuarios suscritos: ' . count($totalSuscripciones) . '</p>'?>
                            <?php endif; ?>
                            <p>
                                <?php if($usuario_esta_suscrito === 'no') : ?>
                                    <a href="suscribete">Suscribete</a>
                                <?php elseif($usuario_esta_suscrito === 'si') : ?>
                                    <a href="unsuscribete">Unsuscribete</a>
                                <?php else : ?>
                                    <b>Para poder suscribirte debes estar conectado.</b>
                                <?php endif; ?>
                            </p>
                        </div>
                    </div>

                    <h5>Seccion de comentarios</h5>
                    <?php if(!empty($comentariosRutina)) : ?>
                        <?php for($i=0; $i<count($comentariosRutina); $i++) :?>
                            <?php if($comentariosRutina[$i]->getRutinaId() === $detallesRutina->getId()) : ?>
                                <?php echo '<p>' . $logged->getNombre() . ' ' . $logged->getApellidos() . ': ' . $comentariosRutina[$i]->getTexto() . '</p>'?>
                            <?php endif; ?>
                        <?php endfor; ?>
                    <?php endif; ?>
                    <p><a href="comentario">Comenta</a></p>
                </div>
            </div>
        </div>
    </div>
